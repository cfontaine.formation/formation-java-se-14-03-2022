package fr.dawan.formation;
public class Main {

    public static void main(String[] args) {      
        // On peut écrire un Thread de 2 manières:

        // 1 - En créant une classe qui implémente l'interface Runnable et redéfinie la méthode run()
        // C'est la méthode qui sera exécutée, quand on va executer le Thread
        
        // Pour instancier le Thread
        MyRunnable run1=new MyRunnable();
        Thread t1=new Thread(run1);
        t1.start();         // Lancer l'execution du thread

        // 2 - En créant une classe qui hérite de Thread et en redéfinissant la méthode run() 
        MyThread th1 = new MyThread(100);
        th1.setName("T1");      // setName permet de nommer le thread 
        MyThread th2 = new MyThread(200);
        th2.setName("T2");
        
        // th2.setDaemon(true); // => le Thread devient un Thread démon: la méthode main va finir même si le Thread n'a pas finis son execution
        
        th1.start();
//        try {
//            th1.join();   // => join: pour continuer l'execution de la méthode main il faut que le thread th2 est fini son exécution 
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        
 //       th1.interrupt();  // interupt va lancer une exception InterruptedException dans le Thread
        th2.start();
    }

}