package fr.dawan.formation;

public class MyRunnable implements Runnable {
    // Le Thread va s'arréter quand la méthode run aura finit son execution
    @Override
    public void run() {

        for (int i = 0; i < 100; i++) {
            System.out.println("Runnable = " + i);
        }

    }

}

