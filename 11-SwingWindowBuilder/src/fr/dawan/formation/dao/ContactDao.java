package fr.dawan.formation.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.dawan.formation.entity.Contact;

// Singleton non thread safe
public final class ContactDao extends AbstractDao<Contact> {

    private static ContactDao instance;

    private ContactDao() {

    }

    public static ContactDao getInstance() {
        if (instance == null) {
            instance = new ContactDao();
        }
        return instance;
    }

    @Override
    protected void remove(long id, Connection cnx) throws SQLException {
        PreparedStatement ps = cnx.prepareStatement("DELETE FROM contacts WHERE id=?");
        ps.setLong(1, id);
        ps.executeUpdate();
    }

    @Override
    protected Contact findById(long id, Connection cnx) throws SQLException {
        Contact cts = null;
        PreparedStatement ps = cnx.prepareStatement("SELECT prenom,nom,date_naissance,email FROM contacts WHERE id=?");
        ps.setLong(1, id);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            cts = new Contact(rs.getString("prenom"), rs.getString("nom"), rs.getString("email"),
                    rs.getDate("date_naissance").toLocalDate());
            cts.setId(id);
        }
        return cts;
    }

    @Override
    protected List<Contact> findAll(Connection cnx) throws SQLException {
        List<Contact> lst = new ArrayList<>();
        Statement stm = cnx.createStatement();
        ResultSet rs = stm.executeQuery("SELECT id,prenom,nom,date_naissance,email FROM contacts");
        while (rs.next()) {
            Contact tmp = new Contact(rs.getString("prenom"), rs.getString("nom"), rs.getString("email"),
                    rs.getDate("date_naissance").toLocalDate());
            tmp.setId(rs.getLong("id"));
            lst.add(tmp);
        }
        return lst;
    }

    @Override
    protected void insert(Contact entity, Connection cnx) throws SQLException {
        PreparedStatement ps = cnx.prepareStatement(
                "INSERT INTO contacts(prenom,nom,email,date_naissance) VALUES (?,?,?,?)",
                Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, entity.getPrenom());
        ps.setString(2, entity.getNom());
        ps.setString(3, entity.getEmail());
        ps.setDate(4, Date.valueOf(entity.getDateNaissance()));
        ps.executeUpdate();
        ResultSet rs = ps.getGeneratedKeys();
        if (rs.next()) {
            entity.setId(rs.getLong(1));
        }
    }

    @Override
    protected void update(Contact entity, Connection cnx) throws SQLException {
        PreparedStatement ps = cnx
                .prepareStatement("UPDATE contacts SET prenom=?,nom=?,email=?,date_naissance=? WHERE id=?");
        ps.setString(1, entity.getPrenom());
        ps.setString(2, entity.getNom());
        ps.setString(3, entity.getEmail());
        ps.setDate(4, Date.valueOf(entity.getDateNaissance()));
        ps.setLong(5, entity.getId());
        ps.executeUpdate();
    }
    
    
    public List<Contact> findByDateNaissance(LocalDate dateNaissance,boolean close){
        List<Contact> lst = new ArrayList<>();
        try {
            Connection cnx=getConnection();
            PreparedStatement ps = cnx.prepareStatement("SELECT id,prenom,nom,date_naissance,email FROM contacts WHERE date_naissance=?");
            ResultSet rs=ps.executeQuery();
            while (rs.next()) {
                Contact tmp = new Contact(rs.getString("prenom"), rs.getString("nom"), rs.getString("email"),
                        rs.getDate("date_naissance").toLocalDate());
                tmp.setId(rs.getLong("id"));
                lst.add(tmp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            closeConnection(close);
        }
        return lst;
    }
}
