package fr.dawan.formation.dao;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import fr.dawan.formation.entity.AbstractEntity;

public abstract class AbstractDao<T extends AbstractEntity> {

    private Properties cnxProperties;

    private Connection cnx;

    public AbstractDao() {
        try (FileInputStream fis = new FileInputStream("db.properties")) {
                cnxProperties = new Properties();
                cnxProperties.load(fis);
            } catch (IOException e) {
                e.printStackTrace();
            }
    }
    
    public void saveOrUpdate(T entity, boolean close) {
        try {
            if (entity.getId() == 0) {
                insert(entity, getConnection());
            } else {
                update(entity, getConnection());
            }
        } catch (SQLException e) {
            e.printStackTrace();
            // relance exception
        } finally {
            closeConnection(close);
        }
    }

    public void remove(T entity, boolean close) {
        remove(entity.getId(), close);
    }

    public void remove(long id, boolean close) {
        try {
            remove(id, getConnection());
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection(close);
        }
    }

    public T findById(long id, boolean close) {
        T e = null;
        Connection c = getConnection();
        try {
            e = findById(id, c);
        } catch (SQLException e1) {
            e1.printStackTrace();
        } finally {
            closeConnection(close);
        }
        return e;
    }

    public List<T> findAll(boolean close) {
        List<T> lst = new ArrayList<>();
        try {
            lst = findAll(getConnection());
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection(close);
        }
        return lst;
    }

    protected abstract void insert(T entity, Connection cnx) throws SQLException;

    protected abstract void update(T entity, Connection cnx) throws SQLException;

    protected abstract void remove(long id, Connection cnx) throws SQLException;

    protected abstract T findById(long id, Connection cnx) throws SQLException;

    protected abstract List<T> findAll(Connection cnx) throws SQLException;

    protected Connection getConnection() {
        if (cnx == null) {
            try {
                Class.forName(cnxProperties.getProperty("driver"));
                cnx = DriverManager.getConnection(cnxProperties.getProperty("url"), cnxProperties.getProperty("userdb"),
                        cnxProperties.getProperty("passworddb"));
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return cnx;
    }

    protected void closeConnection(boolean close ) {
        if (close && cnx != null) {
            try {
                cnx.close();
                cnx = null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
