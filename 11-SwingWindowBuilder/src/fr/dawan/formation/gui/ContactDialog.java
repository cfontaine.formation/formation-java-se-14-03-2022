package fr.dawan.formation.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import fr.dawan.formation.entity.Contact;

public class ContactDialog extends JDialog implements ActionListener {

    private static final long serialVersionUID = 1L;

    private final JPanel contentPanel = new JPanel();
    private JTextField prenomText;
    private JTextField nomText;
    private JTextField emailText;
    private JTextField dateNaissanceText;
    private JLabel errorLabel;

    private Contact contact;

    /**
     * Create the dialog.
     */
    public ContactDialog() {
        setTitle("Ajouter un contact");
        setBounds(100, 100, 370, 220);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(new GridLayout(4, 2, 0, 10));

        JLabel prenomLabel = new JLabel("Prénom");
        contentPanel.add(prenomLabel);
        prenomText = new JTextField();
        contentPanel.add(prenomText);
        prenomText.setColumns(10);

        JLabel lblNewLabel_1 = new JLabel("Nom");
        contentPanel.add(lblNewLabel_1);
        nomText = new JTextField();
        contentPanel.add(nomText);
        nomText.setColumns(10);

        JLabel lblNewLabel_2 = new JLabel("Email");
        contentPanel.add(lblNewLabel_2);
        emailText = new JTextField();
        contentPanel.add(emailText);
        emailText.setColumns(10);

        JLabel lblNewLabel_3 = new JLabel("Date de naissance");
        contentPanel.add(lblNewLabel_3);
        dateNaissanceText = new JTextField();
        contentPanel.add(dateNaissanceText);
        dateNaissanceText.setColumns(10);

        JPanel buttonPane = new JPanel();
        buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
        getContentPane().add(buttonPane, BorderLayout.SOUTH);

        errorLabel = new JLabel("");
        // errorLabel.setEnabled(false);
        errorLabel.setHorizontalAlignment(SwingConstants.LEFT);
        errorLabel.setForeground(Color.RED);
        buttonPane.add(errorLabel);

        JButton okButton = new JButton("OK");
        okButton.setActionCommand("OK");
        okButton.addActionListener(this);
        buttonPane.add(okButton);
        getRootPane().setDefaultButton(okButton);

        JButton cancelButton = new JButton("Anuler");
        cancelButton.setActionCommand("Cancel");
        cancelButton.addActionListener(this);
        buttonPane.add(cancelButton);
    }

    /**
     * Launch Dialog Box
     */
    public ContactDialog(Contact contact) {
        this();
        setTitle("Modifier un contact");
        this.contact = contact;
        prenomText.setText(contact.getPrenom());
        nomText.setText(contact.getNom());
        emailText.setText(contact.getEmail());
        dateNaissanceText.setText(contact.getDateNaissance().toString());
    }

    public Contact showDialog(Component parent) {
        setModal(true);
        setLocationRelativeTo(parent);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setVisible(true);
        return contact;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("OK")) {
            if (checkEmpty()) {
                errorLabel.setText("Des champs obligatoire sont vides");
            } else {
                LocalDate jdn = null;

                try {
                    jdn = LocalDate.parse(dateNaissanceText.getText());
                    if (jdn.isAfter(LocalDate.now())) {
                        errorLabel.setText("La date de naissance doit être passé");
                    } else {
                        if (contact == null) {
                            contact = new Contact();
                        }
                        contact.setPrenom(prenomText.getText());
                        contact.setNom(nomText.getText());
                        contact.setEmail(emailText.getText());
                        contact.setDateNaissance(jdn);
                        setVisible(false);
                        dispose();
                    }
                } catch (DateTimeParseException e1) {
                    errorLabel.setText("La date de naissance n'est pas valide");
                }
            }
        } else if (e.getActionCommand().equals("Cancel")) {
            contact = null;
            setVisible(false);
            dispose();
        }
    }

    private boolean checkEmpty() {
        boolean err = true;
        err &= prenomText.getText().equals("");
        err &= nomText.getText().equals("");
        err &= emailText.getText().equals("");
        err &= dateNaissanceText.getText().equals("");
        return err;
    }

}
