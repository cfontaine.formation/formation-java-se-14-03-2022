package fr.dawan.formation.gui;

import java.time.LocalDate;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import fr.dawan.formation.entity.Contact;

public class ContactTableModel extends AbstractTableModel {

    private static final long serialVersionUID = 1L;

    private List<Contact> contacts;
    
    public ContactTableModel(List<Contact> contacts) {
        this.contacts = contacts;
    }

    @Override
    public int getRowCount() {
        return contacts.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Contact row = contacts.get(rowIndex);
        switch (columnIndex) {
        case 0:
            return row.getPrenom();
        case 1:
            return row.getNom();
        case 2:
            return row.getEmail();
        case 3:
            return row.getDateNaissance();
        }
        return null;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
        case 0:
            return "Prénom";
        case 1:
            return "Nom";
        case 2:
            return "Email";
        case 3:
            return "Date de naissance";
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == 3) {
            return LocalDate.class;
        } else {
            return String.class;
        }
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

}
