package fr.dawan.formation.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import fr.dawan.formation.dao.ContactDao;
import fr.dawan.formation.entity.Contact;

public class Application {

    private JFrame frmCarnetDadresse;
    private JTable table;
    private JButton btnModifier = new JButton("Modifier");
    private JButton btnSupprimer = new JButton("Supprimer");
    private ContactTableModel model;
    private ContactDao dao = ContactDao.getInstance();

    /**
     * Launch the application.
     * 
     * @throws UnsupportedLookAndFeelException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws ClassNotFoundException
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Application window = new Application();
                    window.frmCarnetDadresse.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        if (args.length == 1) {
            setLookAndFeel(args[0]);
        }
    }

    /**
     * Create the application.
     */
    public Application() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frmCarnetDadresse = new JFrame();
        frmCarnetDadresse.setTitle("Carnet d'adresse");
        frmCarnetDadresse.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                close();
            }
        });
        frmCarnetDadresse.setSize(800, 600);
        frmCarnetDadresse.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

        JPanel panel = new JPanel();
        FlowLayout flowLayout = (FlowLayout) panel.getLayout();
        flowLayout.setHgap(20);
        frmCarnetDadresse.getContentPane().add(panel, BorderLayout.SOUTH);

        JButton bpAjouter = new JButton("Ajouter");
        bpAjouter.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ContactDialog dialog = new ContactDialog();
                Contact contact = dialog.showDialog(frmCarnetDadresse);
                if (contact != null) {
                    dao.saveOrUpdate(contact, true);
                    model.getContacts().add(contact);
                    model.fireTableDataChanged();
                }
            }
        });
        panel.add(bpAjouter);

        btnModifier.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int rowSelected = table.getSelectedRow();
                ContactDialog dialog = new ContactDialog(model.getContacts().get(rowSelected));
                Contact contact = dialog.showDialog(frmCarnetDadresse);
                if (contact != null) {
                        dao.saveOrUpdate(contact, true);
                        model.getContacts().set(rowSelected, contact);
                        model.fireTableDataChanged();
                }
            }
        });
        panel.add(btnModifier);
        btnModifier.setEnabled(false);

        btnSupprimer.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (JOptionPane.showConfirmDialog(frmCarnetDadresse, "Voulez vous supprimer le contact ?",
                        "Suppression contact", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                    int rowSelected = table.getSelectedRow();
                    dao.remove(model.getContacts().get(rowSelected), true);
                    model.getContacts().remove(model.getContacts().get(rowSelected));
                    model.fireTableDataChanged();
                }
            }
        });
        panel.add(btnSupprimer);
        btnSupprimer.setEnabled(false);

        JButton btnQuitter = new JButton("Quitter");
        btnQuitter.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                close();
            }
        });
        panel.add(btnQuitter);

        JScrollPane scrollPane = new JScrollPane();
        frmCarnetDadresse.getContentPane().add(scrollPane, BorderLayout.CENTER);

        table = new JTable();
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        model = new ContactTableModel(dao.findAll(true));
        table.setModel(model);
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                boolean selected = table.getSelectedRow() >= 0;
                btnModifier.setEnabled(selected);
                btnSupprimer.setEnabled(selected);
            }
        });
        scrollPane.setViewportView(table);
    }

    private void close() {
        if (JOptionPane.showConfirmDialog(frmCarnetDadresse, "Voulez vous quitter le programme ?", "Quitter",
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            frmCarnetDadresse.setVisible(false);
            frmCarnetDadresse.dispose();
            System.exit(0);
        }
    }

    private static void setLookAndFeel(String lnf) {
        try {
            switch (lnf) {
            case "windows":
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
                break;
            case "motif":
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
                break;
            case "gtk":
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
                break;
            default:
                UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
            }
        } catch (Exception e) {
            System.err.println("Error setting the LookandFeel " + e);
        }
    }
}
