package fr.dawan.formation;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.List;

import fr.dawan.formation.beans.Contact;
import fr.dawan.formation.beans.ContactGen;
import fr.dawan.formation.dao.ContactDao;
import fr.dawan.formation.dao.ContactGenDao;

public class Application {

    public static void main(String[] args) {
        Contact c=new Contact("John","Doe","jd@dawan.fr",LocalDate.now());
        System.out.println(c); // id =0
        testJdbc(c);
        System.out.println(c);  // id généré par la bdd

        
        testDao();
        
        testDaoGenerique();
    }
    
    public static void testJdbc(Contact c) {
        Connection cnx=null;
        try {
            // Chargement du driver de la base de donnée
            Class.forName("org.h2.Driver");
            
            // Création de la connection
            cnx=DriverManager.getConnection("jdbc:h2:C:/Dawan/Formations/formation;AUTO_SERVER=TRUE;AUTO_SERVER_PORT=9090", "sa", "");
           
            cnx.setAutoCommit(false); // Transaction -> désactivation de l'autocommit
           
            // Création de la requète => Statement / PreparedStatement
            PreparedStatement ps=cnx.prepareStatement("INSERT INTO contacts(prenom,nom,email,date_naissance) VALUES (?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
            // Statement.RETURN_GENERATED_KEYS => permet de récupérer après l'execution de la requète la valeur de la clé primaire générer par la base de donnée 

            // Données des valeurs aux paramètres (commence à 1)
            ps.setString(1, c.getPrenom());
            ps.setString(2,c.getNom());
            ps.setString(3, c.getEmail());
            ps.setDate(4, Date.valueOf(c.getDateNaissance()));
            
            ps.executeUpdate(); // execution de la requète
            cnx.commit(); // Transaction -> valider les requêtes
            
            // getGeneratedKeys récupération de la clé primaire générer par la base de donnée
            ResultSet rs=ps.getGeneratedKeys();
            if(rs.next()) {
                c.setId(rs.getLong(1));
            }
            
            // Lecture en base de donnée
            Statement stm=cnx.createStatement();
            // ResultSet contient le resultat de la requète SELECT
            ResultSet rs1=stm.executeQuery("SELECT * FROM contacts");
            while(rs1.next()) { // next retourne vrai tant qu'il reste des lignes dans l'objet ResultSet
                System.out.println(rs1.getLong("id"));
                System.out.println(rs1.getString("prenom"));
                System.out.println(rs1.getString("nom"));
                System.out.println(rs1.getString("email"));
                System.out.println(rs1.getDate("date_naissance"));
            }
            cnx.commit();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            if (cnx != null) {
                try {
                    cnx.rollback(); // Transaction -> annuler les requêtes
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        }
        finally {
            if(cnx!=null) {
                try {
                    cnx.close(); // Fermer la connection
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    static void testDao() {
        Contact c=new Contact("Jane","Doe","jad@dawan.fr",LocalDate.now());
        ContactDao dao=new ContactDao("db.properties");
        dao.saveOrUpdate(c, false);
        List<Contact> lst=dao.findAll(false);
        for(Contact cts: lst)
        {
            System.out.println(cts);
        }
        c.setDateNaissance(LocalDate.of(2000, 1, 1));
        dao.saveOrUpdate(c, false);
        System.out.println(dao.findById(c.getId(), false));
        dao.remove(c, false);
        lst=dao.findAll(true);
        for(Contact cts: lst)
        {
            System.out.println(cts);
        }  
    }
    
    static void testDaoGenerique() {
        ContactGen  c=new ContactGen("Alan","Smithee","asmithee@dawan.fr",LocalDate.now());
        ContactGenDao dao=ContactGenDao.getInstance();
        dao.saveOrUpdate(c, false);
        List<ContactGen> lst=dao.findAll(false);
        for(ContactGen cts: lst)
        {
            System.out.println(cts);
        }
        c.setDateNaissance(LocalDate.of(2000, 1, 1));
        dao.saveOrUpdate(c, false);
        System.out.println(dao.findById(c.getId(), false));
        dao.remove(c, false);
        lst=dao.findAll(true);
        for(ContactGen cts: lst)
        {
            System.out.println(cts);
        }
    }
}
