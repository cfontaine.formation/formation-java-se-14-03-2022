package fr.dawan.formation.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.dawan.formation.beans.ContactGen;

// Singleton non thread safe
public final class ContactGenDao extends AbstractDao<ContactGen> {

    private static ContactGenDao instance;

    private ContactGenDao() {

    }

    public static ContactGenDao getInstance() {
        if (instance == null) {
            instance = new ContactGenDao();
        }
        return instance;
    }

    @Override
    protected void remove(long id, Connection cnx) throws SQLException {
        PreparedStatement ps = cnx.prepareStatement("DELETE FROM contacts WHERE id=?");
        ps.setLong(1, id);
        ps.executeUpdate();
    }

    @Override
    protected ContactGen findById(long id, Connection cnx) throws SQLException {
        ContactGen cts = null;
        PreparedStatement ps = cnx.prepareStatement("SELECT prenom,nom,date_naissance,email FROM contacts WHERE id=?");
        ps.setLong(1, id);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            cts = new ContactGen(rs.getString("prenom"), rs.getString("nom"), rs.getString("email"),
                    rs.getDate("date_naissance").toLocalDate());
            cts.setId(id);
        }
        return cts;
    }

    @Override
    protected List<ContactGen> findAll(Connection cnx) throws SQLException {
        List<ContactGen> lst = new ArrayList<>();
        Statement stm = cnx.createStatement();
        ResultSet rs = stm.executeQuery("SELECT id,prenom,nom,date_naissance,email FROM contacts");
        while (rs.next()) {
            ContactGen tmp = new ContactGen(rs.getString("prenom"), rs.getString("nom"), rs.getString("email"),
                    rs.getDate("date_naissance").toLocalDate());
            tmp.setId(rs.getLong("id"));
            lst.add(tmp);
        }
        return lst;
    }

    @Override
    protected void insert(ContactGen entity, Connection cnx) throws SQLException {
        PreparedStatement ps = cnx.prepareStatement(
                "INSERT INTO contacts(prenom,nom,email,date_naissance) VALUES (?,?,?,?)",
                Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, entity.getPrenom());
        ps.setString(2, entity.getNom());
        ps.setString(3, entity.getEmail());
        ps.setDate(4, Date.valueOf(entity.getDateNaissance()));
        ps.executeUpdate();
        ResultSet rs = ps.getGeneratedKeys();
        if (rs.next()) {
            entity.setId(rs.getLong(1));
        }
    }

    @Override
    protected void update(ContactGen entity, Connection cnx) throws SQLException {
        PreparedStatement ps = cnx
                .prepareStatement("UPDATE contacts SET prenom=?,nom=?,email=?,date_naissance=? WHERE id=?");
        ps.setString(1, entity.getPrenom());
        ps.setString(2, entity.getNom());
        ps.setString(3, entity.getEmail());
        ps.setDate(4, Date.valueOf(entity.getDateNaissance()));
        ps.setLong(5, entity.getId());
        ps.executeUpdate();
    }
    
    
    public List<ContactGen> findByDateNaissance(LocalDate dateNaissance,boolean close){
        List<ContactGen> lst = new ArrayList<>();
        try {
            Connection cnx=getConnection();
            PreparedStatement ps = cnx.prepareStatement("SELECT id,prenom,nom,date_naissance,email FROM contacts WHERE date_naissance=?");
            ResultSet rs=ps.executeQuery();
            while (rs.next()) {
                ContactGen tmp = new ContactGen(rs.getString("prenom"), rs.getString("nom"), rs.getString("email"),
                        rs.getDate("date_naissance").toLocalDate());
                tmp.setId(rs.getLong("id"));
                lst.add(tmp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            closeConnection(close);
        }
        return lst;
    }
}
