package fr.dawan.formation.dao;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import fr.dawan.formation.beans.Contact;

public class ContactDao {

    private Properties cnxProperties;

    private Connection cnx;

    public ContactDao(String fichier) {
        try (FileInputStream fis = new FileInputStream(fichier)) {
            cnxProperties = new Properties();
            cnxProperties.load(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveOrUpdate(Contact entity, boolean close) {
        if (entity.getId() == 0) {
            insert(entity, close);
        } else {
            update(entity, close);
        }
    }

    public void remove(Contact entity, boolean close) {
        remove(entity.getId(), close);
    }

    public void remove(long id, boolean close) {
        try {
            Connection c = getConnection();
            PreparedStatement ps = c.prepareStatement("DELETE FROM contacts WHERE id=?");
            ps.setLong(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection(close);
        }
    }

    public Contact findById(long id, boolean close) {
        Contact cts = null;
        try {
            Connection c = getConnection();
            PreparedStatement ps = c
                    .prepareStatement("SELECT prenom,nom,date_naissance,email FROM contacts WHERE id=?");
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                cts = new Contact(rs.getString("prenom"), rs.getString("nom"), rs.getString("email"),
                        rs.getDate("date_naissance").toLocalDate());
                cts.setId(id);
            }
        } catch (SQLException e1) {
            e1.printStackTrace();
        } finally {
            closeConnection(close);
        }
        return cts;
    }

    public List<Contact> findAll(boolean close) {
        List<Contact> lst = new ArrayList<>();
        try {
            Connection c = getConnection();
            Statement stm = c.createStatement();
            ResultSet rs = stm.executeQuery("SELECT id,prenom,nom,date_naissance,email FROM contacts");
            while (rs.next()) {
                Contact tmp = new Contact(rs.getString("prenom"), rs.getString("nom"), rs.getString("email"),
                        rs.getDate("date_naissance").toLocalDate());
                tmp.setId(rs.getLong("id"));
                lst.add(tmp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
        }
        return lst;
    }

    private void insert(Contact entity, boolean close) {
        try {
            Connection c = getConnection();
            PreparedStatement ps = c.prepareStatement(
                    "INSERT INTO contacts(prenom,nom,email,date_naissance) VALUES (?,?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, entity.getPrenom());
            ps.setString(2, entity.getNom());
            ps.setString(3, entity.getEmail());
            ps.setDate(4, Date.valueOf(entity.getDateNaissance()));
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                entity.setId(rs.getLong(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection(close);
        }
    }

    private void update(Contact entity, boolean close) {
        try {
            Connection c = getConnection();
            PreparedStatement ps = c.prepareStatement("UPDATE contacts SET prenom=?,nom=?,email=?,date_naissance=? WHERE id=?");
            ps.setString(1, entity.getPrenom());
            ps.setString(2, entity.getNom());
            ps.setString(3, entity.getEmail());
            ps.setDate(4, Date.valueOf(entity.getDateNaissance()));
            ps.setLong(5, entity.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection(close);
        }

    }

    private Connection getConnection() {
        try {
            Class.forName(cnxProperties.getProperty("driver"));
            cnx = DriverManager.getConnection(cnxProperties.getProperty("url"), cnxProperties.getProperty("userdb"),
                    cnxProperties.getProperty("passworddb"));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cnx;
    }

    private void closeConnection(boolean close) {
        if (close && cnx != null) {
            try {
                cnx.close();
                cnx = null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
