package fr.dawan.formation;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class TestSwing {

    public static void main(String[] args) {
        // 1 =>Création d'une fenêtre
        JFrame frame = new JFrame("Test Swing"); // en paramètre du constructeur le titre de la fenêtre 
        // setSize => définir les dimensions de la fenêtre
        frame.setSize(800, 600);    
        // setMinimumSize => taille minimal de la fenêtre
        frame.setMinimumSize(new Dimension(300, 200)); 
        // setDefaultCloseOperation => définit se qui est fait lorsque l'on fenêtre
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // <= On arrete le programme quand on ferme la fenêtre

        // 2 => Ajouter un composant dans la fenetre
        JButton bp1=new JButton("Bouton 1"); // Création d'un bouton
        // frame.getContentPane().add(bp1); // ajout du composant dans la fenêtre
       
        // 3 => Positionnement des composants
        // LayoutManagers utilisent des politiques différentes pour le placement des composants
        
        // 3.1 -> Positionnement absolue
//        frame.setLayout(null); // pour un positionnement absolue
//        bp1.setBounds (50, 50, 200, 100); // setBounds =>positionnement (50,50) coin supérieur gauche
//                                          // et dimmension du composant 200 => largeur, 100 => hauteur
//        frame.getContentPane().add(bp1);  // ajout du composant dans la fenêtre
        
        // positionnement absolue -> Problème lorsque l'on redimensionne la fenêtre
        // Les LayoutManager permettent d'adapter le positionnement des composants, quand on redimmensionne la fenêtre
        
        // 3.2 -> FlowLayout
        // Les composants sont placées de gauche à droite jusqu'à ce que la ligne soit remplie
//        // frame.getContentPane().setLayout(new FlowLayout()); // par défaut, les composants sont centrés
//        // frame.getContentPane().setLayout(new FlowLayout(FlowLayout.LEFT)); // placé à gauche => FlowLayout.LEFT, au centre => FlowLayout.CENTER, à droite => FlowLayout.RIGHT
//        frame.getContentPane().setLayout(new FlowLayout(FlowLayout.LEFT,10,30)); // distance entre les composants (gap) horizontal-> 10, vertical->30 
//        frame.getContentPane().add(bp1);
//        bp1.setPreferredSize(new Dimension(200,100)); // Dimmension préférée du bouton bp1
//        frame.getContentPane().add(new JButton("Bouton 2"));
//        frame.getContentPane().add(new JButton("Bouton 3"));
        
        // 3.3 -> BorderLayout
        // Il divise le conteneur en 5 régions (NORTH,SOUTH,EAST,WEST,CENTER), un seul composant par région (qui occupe toute la place)
//        // frame.getContentPane().setLayout(new BorderLayout());
//        frame.getContentPane().setLayout(new BorderLayout(10,20));   // distance entre les composants (gap) horizontal-> 10, vertical-> 20 
//        frame.getContentPane().add(bp1,BorderLayout.NORTH); // Quand on ajoute un composant, on précise dans quelle région
//        frame.getContentPane().add(new JButton("Bouton gauche"),BorderLayout.WEST);
//        frame.getContentPane().add(new JButton("Bouton centre"),BorderLayout.CENTER);
        
        // 3.4 -> GridLayout
//      //  GridLayout gridLayout = new GridLayout();
//      //  gridLayout.setRows(4); // Nb de composant par ligne -> 4
//      //  gridLayout.setColumns(4); // Nb ce composant par colonne -> 4
//      //  gridLayout.setHgap(20); // distance horizontal entre les composants  -> 20
//      //  gridLayout.setVgap(20); // distance vertical entre les composants  -> 20
//      //  frame.getContentPane().setLayout(gridLayout);
//        // ou
//        frame.getContentPane().setLayout(new GridLayout(4,4,20,20));// nb de ligne:4, nb de colonne:4 , distance entre composant horizontal -> 20 , vertical -> 20 
//        for(int i= 0;i<16;i++) { // ajout de bouton
//          frame.getContentPane().add(new JButton(Integer.toString(i+1)));
//        }
        
        // Il existe d'autres Layouts: CardLayout, GridBagLayout ...
        
        // 4 ->JPanel -> Panneau:  conteneur de composant
        // Un panneau à son propre layout => en combinant les JPanels avec différent LayoutManager, on peut organiser le placement des composants dans la fenêtre  
        // BorderLayout pour la fenêtre
        // FlowLayout pour le panneau
        frame.getContentPane().setLayout(new BorderLayout());
        JPanel pan1=new JPanel();
        pan1.setLayout(new FlowLayout(FlowLayout.CENTER,20,10));
        // ajout du panneau dans la "région" SOUTH de la fenêtre
        frame.getContentPane().add(pan1,BorderLayout.SOUTH);
        // On ajoute les boutons au panneau pan1
        bp1.setPreferredSize(new Dimension(150,50));
        pan1.add(bp1);
        JButton bp2=new JButton("Bouton 2");
        bp2.setPreferredSize(new Dimension(150,50));
        pan1.add(bp2);
        
        // 5 -> Gestion des événements
        // Quand on clique sur un bouton, quand déplace le pointeur de la souris sur un composant, quand on appuie sur une touche
        // => Il y a des événements qui sont générés
        // Pour intercepter un événement, un listener doit être associé au composant
        // Le listener est appelé lors de l'évènement, Il peut y avoir plusieurs listener pour un même événement sur un même composant
        // Un listener peut etre associé à plusieurs composants
        
        // ActionnerListener => gestion des évenements action (ex: lorsque l'on clique sur un bouton)
        
//        // Ajouter un ActionListener au bouton bp1
//        bp1.addActionListener(new ActionListener() {
//            
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                JOptionPane.showMessageDialog(frame, "Action","Titre", JOptionPane.INFORMATION_MESSAGE);
//            }
//        });
//        
//        // Ajouter un MouseListener au bouton bp1
//        bp1.addMouseListener(new MouseAdapter() { 
//            // MouseAdapter Classe abstraite qui implemente l'interface MouseListener => pour eviter de redéfinir toutes les méthodes
//           
//            @Override
//            public void mouseEntered(MouseEvent e) { 
//                // Affiche la coordonée x à chaque fois que le pointeur de la souris entre sur le bouton bp1 
//                System.out.println("Enter " + e.getX());
//            }
//        });

        
        // Associer une commande à un action
        BoutonAction ba=new BoutonAction();
        bp1.addActionListener(ba);
        bp1.setActionCommand("bp1");
        bp2.addActionListener(ba);
        bp2.setActionCommand("bp2");
        
        // Affichage de la fenêtre
        frame.setVisible(true); 
        
        // 6 -> Boite de dialogue
        // 6.1 -> Boite de message
        JOptionPane.showMessageDialog(frame,    // Composant parent
                "Messsage", // Message
                "Titre",    // Titre
                JOptionPane.WARNING_MESSAGE); // type de message
        // 6.2 -> Boite de confirmation
        int reponse=JOptionPane.showConfirmDialog(frame,
                "Message",
                "Titre",
                JOptionPane.OK_CANCEL_OPTION, // YES_NO_OPTION, YES_NO_CANCEL_OPTION ou OK_CANCEL_OPTION
                JOptionPane.QUESTION_MESSAGE); 
        System.out.println("Réponse=" + reponse + " Ok= " + JOptionPane.OK_OPTION + "Cancel= " + JOptionPane.CANCEL_OPTION);
        
        // 6.3 -> Boite de saisie de texte
        String text=JOptionPane.showInputDialog
                (frame,
                "Message",
                "Titre",
                JOptionPane.INFORMATION_MESSAGE
                );
        System.out.println(text); // Ok => retourne la chaine saisie, Annuler => retourne null
        
        // Boite de dialogue personalisable (on peut y ajouter des composants) => JDialog
    }

}
