package fr.dawan.formation;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class Application {

    public static void main(String[] args) {

        // Checked exception => on est obligé (par java) de traiter l'exception
        try {
            // ...
            // ouverture d'un fichier qui n'existe pas => lance une exception  FileNotFoundException
            FileInputStream fo = new FileInputStream("nexistepas");
            int a = fo.read(); // read peut lancer une IOException
            System.err.println("Suite du code");   
        } 
        // pour un bloc try, il peut y avoir plusieurs blocs catch pour traiter 
        // diférentes exceptions de la - à la + générale
        catch (FileNotFoundException e) { // si une exception FileNotFoundException ce produit dans le bloc try
            System.err.println("Le fichier n'existe pas");    
        } 
        catch(IOException e) {  
            System.err.println("Lecture impossible");
        }
        catch (Exception e) {
            System.err.println("Un autre exception");
        } finally {         // le bloc finally est toujours exécuté
            System.err.println("Libérer les ressources");
        }

        // Runtime Exception => on peut traiter l'exception ou pas (pas d'obligation)
        try {
            int[] tab = new int[5];
            tab[1] = 3; // 10 =>ArrayIndexOutOfBoundsException
            String str = null;
            String strUpper = str.toUpperCase();
            System.out.println(strUpper);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.err.println("Indice en dehors du tableau:" + e.getMessage());
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        Scanner sc = new Scanner(System.in);
        int age = sc.nextInt();
        try {
            traitementEmploye(age);
            System.out.println("Suite traitement");
        } catch (Exception e) {
            System.out.println("Traitement main de l'exception");
            e.printStackTrace();
        }

        System.out.println("fin de programme");
        sc.close();
    }

    public static void traitementEmploye(int age) throws EmployeException {
        System.out.println("Debut traitement Employe");
        try {
            traitementAge(age);
        } catch (AgeNegatifException e) {
            System.out.println("Traitement partiel de l'exception");
            // throw e; // relance de l'exception
            throw new EmployeException("Erreur employe", e); // relance d'une exception diférente
        }
        System.out.println("Fin traitement Employe");
    }

    public static void traitementAge(int age) throws AgeNegatifException {
        System.out.println("Debut traitement Age");
        if (age < 0) {
            throw new AgeNegatifException(age);
        }
        System.out.println("Fin traitement");
    }

}
