package fr.dawan.formation;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

// Il faut lancer l'execution du serveur et excecuter plusieurs fois la classe Client
//pour fermer un client, il faut saisir quit
//il faut forcer l'arrêt du serveur => boucle infinie

public class Server {

 void launch() {
     
     // ServerSocket implémente Autocloasable
     try (ServerSocket servSocket = new ServerSocket(15001)){
          // Le server socket écoute le port 15001 et attend qu'un client (socket) se connecte
         boolean loop = true;
         while (loop) {
             Socket s = servSocket.accept(); // Lorsqu'un client se connecte la méthode accept retourne un socket
             ThreadServerClient sct = new ThreadServerClient(s); // On crée un thread qui va gérer la connection avec le client
             sct.start();
         }
     } catch (IOException e) {
          e.printStackTrace();
     }
 }
 
 public static void main(String[] args) {
     Server server = new Server();
     server.launch();

 }

}

