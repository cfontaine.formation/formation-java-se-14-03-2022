package fr.dawan.formation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    public void launch() {
        Scanner scan = new Scanner(System.in);
        // Création d'un socket qui va se connecter au serveur sur le port 15001 de
        // l'adresse localhost
        try (Socket socket = new Socket(InetAddress.getLoopbackAddress(), 15001);
                PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
                BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));) {

            String msg = "";
            while (!msg.equals("quit")) {
                System.out.print(">>");
                msg = scan.nextLine();

                // envoie du message saisie sur le flux
                writer.println(msg);

                // attente et affichage du message écho renvoyer par le serveur.
                System.out.println(reader.readLine());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        scan.close();
    }

    public static void main(String[] args) {
        Client client = new Client();
        client.launch();
    }

}
