import java.util.Objects;


// Animal est une classe abstraite, elle ne peut pas être instantiée elle même
// mais uniquement par l'intermédiaire de ses classes filles
public abstract class Animal {
    
    private double poid;
    
    private int age;

    public Animal() {
    }

    public Animal(double poid, int age) {
        this.poid = poid;
        this.age = age;
    }

    public double getPoid() {
        return poid;
    }

    public void setPoid(double poid) {
        this.poid = poid;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    
 // Méthode abstraite qui doit obligatoirement redéfinit par les classes filles
    public abstract void emmettreSon() ;
//    public void emmettreSon() 
//    {
//        System.out.println("L'animal emet un son");
//    }

    // Redéfinition de la méthode toString de Object
    @Override
    public String toString() {
        return "Animal [poid=" + poid + ", age=" + age + "]";
    }

    // Redéfinition de la méthode hashCode et de equals 
    @Override
    public int hashCode() {
        return Objects.hash(age, poid);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Animal other = (Animal) obj;
        return age == other.age && Double.doubleToLongBits(poid) == Double.doubleToLongBits(other.poid);
    }

    
}
