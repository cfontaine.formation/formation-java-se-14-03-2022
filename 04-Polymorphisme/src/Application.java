public class Application {

    public static void main(String[] args) {
//        Animal a1=new Animal(3.5,4);   // Impossible la classe est abstraite
//        a1.emmettreSon();
        
        Chien ch1=new Chien(4.5,3,"Rolo");
        ch1.emmettreSon();
        
        Animal a2= new Chien(3.5,7,"Laika"); // On peut créer une instance de Chien (classe fille) qui aura une référence Animal (classe mère)
                                             // L'objet Chien sera vu comme un Animal, on ne pourra pas accèder aux méthodes propre au chien (nom,...)
        a2.emmettreSon();                    // C'est la méthode emettreSon de Chien qui sera appelée                    

        System.out.println(a2.getAge());
       
        if(a2 instanceof Chien) {   // test si a2 est de "type" Chien
            Chien ch2= (Chien)a2;   // Pour passer d'une super-classe à une sous-classe, il faut le faire explicitement avec un cast
            ch2.emmettreSon();
            System.out.println(ch2.getNom());   // avec la référence c2 (de type Chien), on a bien accès à toutes les méthodes de la classe Chien
        }
        
        Refuge r=new Refuge();
        r.ajouter(ch1);
        r.ajouter(a2);
        r.ajouter(new Chat(2.4,2,9));
        r.ajouter(new Chat(5.0,5,8));
        r.ecouter();
        
        // Interface
        PeutMarcher pm1=new Chien(4.5,3,"Rolo");    // On peut utiliser une référence vers une interface pour référencer une classe qui l'implémente
        pm1.courir();                               // L'objet chien n'est vu que comme un interface PeutMarcher, on ne peut utiliser que les méthode de PeutMarcher (marcher, courrir)
        
        // Object
        // toString
        Object obj =new Chien(4.5,3,"Rolo");
        //System.out.println(obj.toString());
        System.out.println(obj);
        
        // equals
        Chien ch3=new Chien(4.5,3,"Rolo");  
        Chien ch4=new Chien(4.5,6,"Rolo");
        System.out.println(ch3==ch4);        // false, on compare les références (ch3 et ch4 sont 2 objets, leurs références sont différente) 
        System.out.println(ch3.equals(ch4)); // true, on compare le contenu des 2 objets, si dans la classe Chien equals est redéfinie
       
        // clone
        try {
            Chien ch5=(Chien)ch4.clone();
            System.out.println(ch4);
            System.out.println(ch5);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }

}
