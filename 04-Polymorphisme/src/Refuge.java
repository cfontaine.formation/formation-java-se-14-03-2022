
public class Refuge {
    
    private Animal[] place=new Animal[20];

    private int nbPlaceOccupe;
    
    public Refuge() {
    }

    public Animal[] getPlace() {
        return place;
    }

    public void setPlace(Animal[] place) {
        this.place = place;
    }

    public int getNbPlaceOccupe() {
        return nbPlaceOccupe;
    }

    public void setNbPlaceOccupe(int nbPlaceOccupe) {
        this.nbPlaceOccupe = nbPlaceOccupe;
    } 
    
    public void ajouter(Animal a) {
        if(nbPlaceOccupe<place.length) {
            place[nbPlaceOccupe]=a;
            nbPlaceOccupe++;
        }
    }
    
    public void ecouter() {
        for(int i=0; i<nbPlaceOccupe;i++) {
            place[i].emmettreSon();
        }
    }
    
}
