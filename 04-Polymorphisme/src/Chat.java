
public class Chat extends Animal implements PeutMarcher{
    
    private int nbVie=9;

    public Chat(double poid, int age,int nbVie) {
        super(poid, age);
        this.nbVie=nbVie;
    }

    public int getNbVie() {
        return nbVie;
    }

    public void setNbVie(int nbVie) {
        this.nbVie = nbVie;
    }

    @Override
    public void emmettreSon() {
        System.out.println("Le chat miaule");
    }

    @Override
    public void marcher() {
        System.out.println("Le chat marche");
        
    }

    @Override
    public void courir() {
        System.out.println("le chat court");
        
    }

}
