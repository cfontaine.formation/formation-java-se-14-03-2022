import java.util.Objects;


// La classe Chien hérite de la classe Animal et implémente l'interface PeutMarcher
// Pour pouvoir cloner un  objet avec la méthode clone de Object, il faut redéfinir la méthode clone
// et il faut que la classe impléménte l'interface Cloneable (interface marqueur qui ne contient aucune méthode)

public class Chien extends Animal implements PeutMarcher,Cloneable{

    private String nom;

    public Chien(double poid, int age, String nom) {
        super(poid, age);
        this.nom=nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public void emmettreSon() {
        System.out.println(nom + " aboie");
    }

    @Override
    public void marcher() {  // Impléméntation de la méthode courrir de l'interface  PeutMarcher
        System.out.println(nom + " marche");
        
    }

    @Override
    public void courir() {  // Impléméntation de la méthode marcher de l'interface PeutMarcher
        System.out.println(nom + " court");
        
    }

    @Override
    public String toString() {
        return "Chien [nom=" + nom + " " + super.toString() + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(nom);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Chien other = (Chien) obj;
        return Objects.equals(nom, other.nom);
    }

    // Redéfinition de la méthode clone (on augmente la visibilité de la méthode protected -> public)
    @Override
    public  Object clone() throws CloneNotSupportedException {
            return super.clone();
    }
    
    
}
