
public class CompteEpargne extends CompteBancaire {

    private double taux;
  
    public CompteEpargne(String titulaire,double taux) {
        super(titulaire);
        this.taux=taux;

    }
    
    public CompteEpargne(double solde, String titulaire,double taux) {
        super(solde, titulaire);
        this.taux=taux;
    }

    public void calculInteret() {
        solde=solde*(1+taux/100);
    }

    @Override
    void afficher() {
       super.afficher();
       System.out.println("Taux=" +  taux);
    }  
}
