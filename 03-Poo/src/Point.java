
public class Point {

    private int x;
    private int y;

    // Constructeur par défaut
    public Point() {
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    // getter / setter
    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void afficher() {
        System.out.println("x=" + x + " y=" + y);
    }

    public void translation(int tx, int ty) {
        x += tx;
        y += ty;
    }

    public double norme() {
        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
    }

    public static double distance(Point p1, Point p2) {
        return Math.sqrt(Math.pow((p2.x - p1.x), 2) + Math.pow((p2.y - p1.y), 2));
    }

}
