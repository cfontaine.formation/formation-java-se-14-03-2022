import java.util.Date;
import java.sql.*;  // importe toutes les classes du package java.sql sauf Date

public class Application {

    public static void main(String[] args) {
        System.out.println("Nombre voiture " + Voiture.getNbVoiture());
       
        // Appel d'une méthode de classe
        Voiture.testMethodeDeclasse();
        
        // Instantiation de la classe Voiture
        Voiture v1=new Voiture();
        System.out.println("Nombre voiture " + Voiture.getNbVoiture());
        v1.afficher();
        //v1.marque="Ford"; // n'est plus accessible => privé
        // Encapsulation => Pour modifier les attributs, on doit passer par les setters
        v1.setCouleur("Bleu"); //v1.couleur="Bleu"
        // Encapsulation => Pour accéder aux attributs, on doit passer par les getters
        v1.setVitesse(20); //v1.vitesse=20;
        System.out.println(v1.getMarque() + " " + v1.getVitesse()); //v1.vitesse
        
        // Appel d’une méthode d’instance
        v1.accelerer(20);
        v1.afficher();
        v1.freiner(10);
        v1.afficher();
        System.out.println(v1.estArreter());
        v1.arreter();
        System.out.println(v1.estArreter());
        v1.afficher();
        
        Voiture v2= new Voiture();
        System.out.println("Nombre voiture " + Voiture.getNbVoiture());
       // v2.marque="Honda";
        v2.setVitesse(10);
        System.out.println(v2.getMarque());
        v2.afficher();
        
        Voiture v3=new Voiture("Fiat","Jaune","FR-5962-RT");
        System.out.println("Nombre voiture " + Voiture.getNbVoiture());
        v3.afficher();
        
        System.out.println("Compare vitesse=" + Voiture.egalVitesse(v1, v3));
        
        // v3=null; // il n'y a plus de référence sur l'objet => eligible à la destruction par le garbge collector
        // System.gc(); // Appel explicite du garbage collector => à eviter

        // Agrégation
        Personne per1=new Personne("John","Doe","jdoe@dawan.com",30);
        per1.afficher();
        v1.setProprietaire(per1);
        
        // Package
        Date d1 = new Date();
        java.sql.Date dSql = new java.sql.Date(0);
        ResultSet res = null;
        
        // Héritage
        VoiturePrioritaire vp1=new VoiturePrioritaire(true);
        vp1.afficher(); // Redéfinition de méthode
        vp1.allumerGyro();
        System.out.println(vp1.isGyro());
        vp1.eteindreGyro();
        System.out.println(vp1.isGyro());
        
        VoiturePrioritaire vp2=new VoiturePrioritaire("Opel","Gris","FR-4523-AZ",false);
        vp2.afficher();
        
        // Exercice: Compte Bancaire
        CompteBancaire cb1=new CompteBancaire(100.0,"John Doe");
        //cb1.solde=100.0;
        //cb1.iban="fr5962-0000-0000";
        //cb1.titulaire="John Doe";
        cb1.afficher();
        cb1.crediter(120.0);
        cb1.afficher();
        System.out.println(cb1.estPositif());
        
        CompteBancaire cb2=new CompteBancaire("Jane Doe");
        cb2.afficher();
        
        //  Exercice héritage : CompteEpargne
        CompteEpargne ce1=new CompteEpargne("Alan smithee", 0.75);
        ce1.afficher();
        ce1.crediter(400);
        ce1.calculInteret();
        ce1.afficher();
        
        // Exercice: Point
        Point a= new Point();
        a.afficher();
        Point b= new Point(3,1);
        b.afficher();
        
        a.translation(1, 1);
        a.afficher();
        
        System.out.println("Norme= " + a.norme());
        System.out.println("Distance=  " + Point.distance(a, b));
        
    }

}
