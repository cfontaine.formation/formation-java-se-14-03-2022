
public class CompteBancaire {
    protected  double solde = 50.0;
    private String iban;
    private String titulaire;
    
    private static int cptCompte;
    
    public CompteBancaire(String titulaire) {
        this.titulaire = titulaire;
        cptCompte++;
        iban="fr-5900-"+cptCompte;
    }
    
    public CompteBancaire(double solde, String titulaire) {
        this(titulaire) ;
//      this.titulaire = titulaire;
//      cptCompte++;
//      iban="fr-5900-"+cptCompte;
        this.solde = solde;
    }

    public String getTitulaire() {
        return titulaire;
    }

    public void setTitulaire(String titulaire) {
        this.titulaire = titulaire;
    }

    public double getSolde() {
        return solde;
    }

    public String getIban() {
        return iban;
    }

    void afficher() {
        System.out.println("Solde :"+solde);
        System.out.println("Iban :"+iban);
        System.out.println("Titulaire :"+titulaire);
    }
    
    void crediter(double valeur) {
        if(valeur>0) {
            solde+=valeur;
        }
    }
    
    void debiter(double valeur) {
        if(valeur>0) {
            solde-=valeur;
        }
    }
    
   boolean estPositif() {
        return solde>=0.0;
    }
   
 
}
