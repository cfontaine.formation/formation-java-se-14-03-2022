
public /*final*/ class Voiture {    // final => interdire l'héritage à partir de la classe

    public final static int vitesseMax=120; 
    
    // Variable d'instance (attribut)

    // L'encapsulation consiste à cacher l'état interne d'un objet et d'imposer de
    // passer par des méthodes permettant un accès sécurisé à l'état de l'objet
    // private => attribut accessible seulement dans la classe elle-même
    private String marque;
    private String couleur = "Noir"; // Noir valeur par défaut de l'attribut couleur
    private String plaqueIma;        // par défaut String a pour valeur null (référence)
   //private
    protected int vitesse;             // par défaut int a pour valeur 0
    private int compteurKm = 20;

    // Variable de classe
    private static int nbVoiture;
    
    // Agrégation
    private Personne proprietaire; 

    // Constructeurs
    // Avec eclipse, on peut générer les constructeurs
    // Menu contextuel -> Source -> Generarted constructror using fields...
    
    // Constructeur par défaut (sans paramètres)
    Voiture() {
        System.out.println("Constructeur par défaut: Voiture");
        nbVoiture++;
    }

    // On peut surcharger le constructeur
    Voiture(String marque, String couleur, String plaqueIma) {
        this(); // Appel d'un autre constructeur avec this, doit être la première instruction du constructeur
                // ici on appel le constructeur par défaut
        System.out.println("Constructeur 3 params");
        this.marque = marque;    // utilisation de this pour indiquer que l'on acccède à la variable d'instance
        this.couleur = couleur;
        this.plaqueIma = plaqueIma;
    }

    public Voiture(String marque, String couleur, String plaqueIma, int vitesse, int compteurKm) {
        this(marque, couleur, plaqueIma); // appel du constructeur 3 paramètres
        System.out.println("Constructeur 5 params");
//        this.marque = marque;
//        this.couleur = couleur;
//        this.plaqueIma = plaqueIma;
        this.vitesse = vitesse;
        this.compteurKm = compteurKm;
    }
    
    public Voiture(String marque, String couleur, String plaqueIma, Personne proprietaire) {
        this(marque, couleur, plaqueIma); // appel du constructeur 3 paramètres
//        this.marque = marque;
//        this.couleur = couleur;
//        this.plaqueIma = plaqueIma;
        this.proprietaire = proprietaire;
    }

    // Getter / Setter
    // Un getter permet l'accès en lecture à un attribut
    public int getVitesse() {
        return vitesse;
    }

    // Un setter permet l'accès en écriture à un attribut
    public void setVitesse(int vitesse) {
        if (vitesse > 0) {
            this.vitesse = vitesse;
        }
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public int getCompteurKm() {
        return compteurKm;
    }

    public void setCompteurKm(int compteurKm) {
        this.compteurKm = compteurKm;
    }

    public String getMarque() {
        return marque;
    }

    public String getPlaqueIma() {
        return plaqueIma;
    }

    public static int getNbVoiture() {
        return nbVoiture;
    }
    
    public Personne getProprietaire() {
        return proprietaire;
    }

    public void setProprietaire(Personne proprietaire) {
        this.proprietaire = proprietaire;
    }

    // Méthode d'instance
    void accelerer(int vAcc) {
        if (vAcc > 0) {
            vitesse += vAcc;
        }
    }

    void freiner(int vFrn) {
        if (vFrn > 0) {
            vitesse -= vFrn;
        }
    }

    void arreter() {
        vitesse = 0;
    }

    boolean estArreter() {

        if (vitesse == 0)
            return true;
        else {
            return false;
        }
        // ou
        // return vitesse==0;
    }

   // final void afficher() {   // final => interdire la redéfinition d'une méthode
   void afficher() {
        System.out.println(marque + " " + couleur + " " + plaqueIma + " " + vitesse + " " + compteurKm);
    }

    // Méthode de classe
    static void testMethodeDeclasse() {
        System.out.println("Méthode de classe");
        System.out.println(nbVoiture);
        // Les méthodes et les variables ne peuvent pas être utilisées dans une méthode
        // de classe
        // System.out.println(vitesse); // Erreur=> une méthode de classe ne peut pas accèder à une variable d'instance
        // arreter();                   // ou à une méthode d'instance
    }

    static boolean egalVitesse(Voiture va, Voiture vb) {
        // Dans un méthode de classe on peut accèder à une variable d'instance
        // si la référence d'un objet est passée en paramètre
        return va.vitesse == vb.vitesse;
    }
}
