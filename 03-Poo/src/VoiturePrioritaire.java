
public class VoiturePrioritaire extends Voiture {
    
    private boolean gyro;
    
    public VoiturePrioritaire(boolean gyro) {
        //super(); // pas de d'appel explicite à super
                   // par défaut le contructeur par défaut de la classe mère qui est appeleé 
        System.out.println("Constructure par défaut: Voiture prioritaire");
        this.gyro=gyro;
    }

    public VoiturePrioritaire(String marque, String couleur, String plaqueIma,boolean gyro) {
        super(marque, couleur, plaqueIma); // Appel du constructeur 3 paramètres de la classe mère
                                           // =>Voiture(marque, couleur, plaqueIma)
        System.out.println("Constructure 3 paramètres : Voiture prioritaire");
        this.gyro=gyro;
    }

    public void allumerGyro() {
        gyro=true;
        vitesse=vitesse*2;
    }
    
    public void eteindreGyro() {
        gyro=false;
    }

    public boolean isGyro() {
        return gyro;
    }

    // Redéfinition de la méthode afficher
    @Override
    void afficher() {
        super.afficher(); // appel la méthode afficher de la classe Mère (Voiture)
        System.out.println("Gyro=" +gyro);
    }
    
    
    
}
