// import java.time.LocalDate;

public class DataType {

    // Méthode main => point d'entré du programme
    public static void main(String[] args) {
        // Déclaration variable
        int i;
        
        // System.out.println(i); // En java, on ne peut pas utiliser une variable non initlisée

        // Initialisation de la variable
        i=42;
        System.out.println(i);

        // Déclaration multiple
        double hauteur,largeur;hauteur=1.3;largeur=3.4;
        System.out.println(hauteur+" largeur="+largeur);

        // Déclaration et initialisation d'une variable
        double d=1.23;
        double d1=3.4,d2,d3=4.5;
        d2=12.4;
        System.out.println( d + " " + d1 + " " + d2 + " " +d3);
        
        // Littéral Boolean
        boolean tst=true; // false
        System.out.println(tst);
        
        // Littéral caractère 
        char ch='a';
        char chUtf8='\u0061';
        System.out.println(ch+" "+chUtf8);

        // Littérale entière est par défaut de type int
        long l=12345678911L; // L-> litéral de type long
        System.out.println(l);

        // Littéraux entier changement
        int dec=123;        // décimal base 10
        int hex=0xf4e;      // 0x -> héxadécimal base 16
        int oct=064;        // 0  -> octal base 8
        int bin=0b10011101; // 0b -> binaire base 2
        System.out.println(dec + " " + hex + " " + oct + " " + bin);
        
        // Littéral réel
        double dd1=12.34;
        double dd2=.56;
        double dd3=1.23e3; // 1230
        System.out.println(dd1 + " " + dd2 + " " + dd3);

        // Littéral réel est par défaut de type double
        float f=1.23F; // F -> litéral de type float
        System.out.println(f);
        
        // Transtypage implicite => pas de perte de donnée
        // Type inférieur vers un type supérieur
        short s=12;
        int ti1=s;
        System.out.println(s + " " + ti1);
        
        // Entier vers un réel
        double ti2=ti1;
        System.out.println(ti1 + " " + ti2);
        
        // Transtype explicite cast => (nouveauType)
        // Réel vers entier
        double te1=34.5;
        int ti=(int)te1;
        System.out.println(te1+" "+ti);
        
        // Type supérieur vers un type inférieur
        byte bi=(byte)ti;
        System.out.println(ti+" "+bi);

        // Transtype explicite: Dépassement de capacité
        int to=300;         // 00000000 00000000 00000001 00101100 -> 300
        byte tb=(byte)to;   //                            00101100 -> 44
        System.out.println(to+" "+tb);

        // Type référence
        String str1=new String("Hello"); // ou "Hello"
        String str2=null;   // str2 ne référence aucun objet
        System.out.println(str1+" "+str2);
        
        str2=str1;  // str1 et str2 font références au même objet
        System.out.println(str1+" "+str2);

        str1=null;
        System.out.println(str1+" "+str2);

        str2=null;
        System.out.println(str1+" "+str2);
        // str1 et str2 sont égales à null
        // Il n'y a plus de référence sur l'objet => Il éligible à la destruction par le garbage collector

        // LocalDate date=str1; 
        // => Erreur date et str1 ne font pas référence au même type date => LocalDate et str1 => String
    }

}
