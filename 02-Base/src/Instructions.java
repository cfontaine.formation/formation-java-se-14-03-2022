import java.util.Scanner;

public class Instructions {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        // Condition: if
        double d = 10.0;
        if (d > 10.0) {
            System.out.println("sup à 10");
        } else if (d < 10.0) {
            System.out.println("inf à 10");
        } else {
            System.out.println("égal  à 10");
        }

        // Exercice: Trie de 2 valeurs
        // Saisir 2 nombres et afficher ces nombres triés dans l'ordre croissant
        // sous la forme 1.5 < 10.5
        System.out.println("Entrer 2 nombres à virgule flottante");
        double d1 = sc.nextDouble();
        double d2 = sc.nextDouble();
        if (d1 < d2) {
            System.out.println(d1 + "<" + d2);
        } else {
            System.out.println(d2 + "<" + d1);
        }
        
        // Exercice: Intervalle
        // Saisir un nombre et dire s'il fait parti de l'intervalle -4 (exclus) et 7 (inclus)
        System.out.println("Entrer un nombre entier");
        int v=sc.nextInt();
        if(v>-4 && v<=7) {
            System.out.println(v + "fait partie de l'intervale");
        }
        
        // ou
//        if(v>-4) {
//            if(v<=7) {
//                System.out.println(v + "fait partie de l'intervale");  
//            }
//        }
        
        
        // Opérateur ternaire
        System.out.println("Entrer un nombre entier");
        int vi=sc.nextInt();
        int abs=vi<0?-vi:vi;
        System.out.println("abs=" + abs);
      
        // Condition switch
        System.out.println("Entrer un jour entre 1 et 7");
        int jours = sc.nextInt();
        switch (jours) {

        case 1:
        System.out.println("Lundi");
        break;
        
        case 6:
        case 7:
        System.out.println("week end !");
        break;
        
        default:
        System.out.println("autre jour");
        break;

        }
        
        // Exercice Calculatrice
        // Faire un programme calculatrice
        // Saisir dans la console
        // - un double v1
        // - une chaine de caractère opérateur qui a pour valeur valide : + - * /
        // - un double v2
        // Afficher:
        // - Le résultat de l’opération
        // - Une message d’erreur si l’opérateur est incorrecte
        // - Une message d’erreur si l’on fait une division par 0

        double val1=sc.nextDouble();
        String op=sc.next();
        double val2=sc.nextDouble();
        switch(op) {
        case "+":
            System.out.println( val1 + " + " + val2 + " = "+ (val1 + val2));
            break;
        case "-":
            System.out.println( val1 + " - " + val2 + " = "+ (val1 - val2));
            break;
        case "x":
            System.out.println( val1 + " x " + val2 + " = "+ (val1 * val2));
            break;
        case "/":
            if(val2!=0.0) {
                System.out.println( val1 + " / " + val2 + " = "+ (val1 / val2));
            }
            else {
                System.out.println("Erreur: division par 0");
            }
            break;
            default:
                System.out.println(op +" n'est pas un opérateur valide");
        }
           
        // Boucle while
        int j=0;
        while(j<10) {
            System.out.println("j=" + j);
            j++;
        }
        
        // Boucle do while
        j=0;
        do {
            System.out.println("j="+j);
            j++;
        } while(j<10);
        
        // Boucle for
        for (int i=0;i<10;i++)
        {
            System.out.println("i="+i);
        }
        
        for (int i=10;i>0;i--)
        {
            System.out.println("i="+i);
        }
        
        // Exercice: Table de multiplication
        // Faire un programme qui affiche la table de multiplication pour un nombre entre 1 et 9
        //
        // 1 X 4 = 4
        // 2 X 4 = 8
        // …
        // 9 x 4 = 36
        //
        int m=sc.nextInt();
        for(int i=1;i<10;i++) {
            System.out.println(i + " x " + m +" = " + (i*m));
        }
        
        // Instructions de branchement
        // break
        for (int i=0;i<10;i++)
        {
            if(i==3){
                break;  // break => termine la boucle
            }
            System.out.println("i="+i);
        }
        

        // continue
        for (int i=0;i<10;i++)
        {
            if(i==3){
                continue; // continue => on passe à l'itération suivante
            }
            System.out.println("i="+i);
        }
        
        // LABEL
        // break avec Label:
   EXIT_LOOP: for(int i=0;i<10;i++) {
                for(int l=0;l<10;l++) {
                        
                    if(i==2) {
                         break EXIT_LOOP;    // se branche sur le label EXIT_LOOP et quitte les 2 boucles imbriquées
                    }
                    System.out.println("i=" + i + "j=" + l);
                }
            }
        
        sc.close();
    }

}
