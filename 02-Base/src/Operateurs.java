import java.util.Scanner;

public class Operateurs {

    public static void main(String[] args) {
        // Opérateur arithmétique
        int a = 1;
        int b = 2;
        int c = a + b;
        System.out.println(c);
        System.out.println(3 % 2); // % modulo => reste de la division entière

        // Incrémenration/ Décrémentation
        // Pré-incrémentation
        int inc = 0;
        int res = ++inc; // Incrémentation de inc et affectation de res avec la valeur de inc
        System.out.println("inc=" + inc + " res=" + res); // inc=1 res=1

        // Post-incrémentation
        inc = 0;
        res = inc++; // Affectation de res avec la valeur de inc et incréméntation de inc
        System.out.println("inc=" + inc + " res=" + res); // res=0 inc=1;

        // Affectation composée
        inc = 12;
        inc += 30; // inc=inc+30

        // Opérateur de comparaison
        // Une comparaison a pour résultat un booléen
        a = 23;
        b = 4;
        boolean tst1 = a == b; // false
        boolean tst2 = a != b; // true
        System.out.println(tst1 + " " + tst2);

        // Opérateur logique
        // ! => Opérateur non
        boolean tst3 = !tst1; // true
        System.out.println("tst3=" + tst3);

        // c1       c2    |  ET    |  OU      | Ou exclusif
        //-------------------------------------------------
        // faux     faux  |  faux  |  faux    | faux
        // faux     vrai  |  faux  |  vrai    | vrai
        // vrai     faux  |  faux  |  vrai    | vrai
        // vrai     vrai  |  vrai  |  vrai    | faux

        // Opérateur court-circuit => && et ||
        // && -> Opérateur et
        // && dés qu'une comparaison est fausse, les suivantes ne sont pas évaluées
        boolean tst4 = a < 0 && b == 4; // false
        System.out.println("tst4=" + tst4);

        // || -> Opérateur ou
        // || dés qu'une comparaison est vrai, les suivantes ne sont pas évaluées
        boolean tst5 = a == 23 || b >= 0; // true
        System.out.println("tst5=" + tst5);

        // Opérateur Binaire
        byte bin = 0b00100110;
        System.out.println(Integer.toBinaryString(~bin));               // complémént -> 11011001
        System.out.println(Integer.toBinaryString(bin & 0b00001101));   // et -> 100
        System.out.println(Integer.toBinaryString(bin | 0b00001101));   // ou -> 101111
        System.out.println(Integer.toBinaryString(bin ^ 0b00001101));   // ou exclusif -> 101011

        // Opérateur de décalage
        System.out.println(Integer.toBinaryString(bin << 2));   // décallage à gauche de 2 -> 10011000
        System.out.println(Integer.toBinaryString(bin >> 1));   // décallage à gauche de 1, insère à droite du bit de signe -> 10011
        System.out.println(Integer.toBinaryString(bin >>> 3));  // décallage à gauche de 3, insère à droite un 0 -> 100

        // Promotion numérique
        // 1=> Le type le + petit est promu vers le + grand type des deux
        float prn1 = 12.3F;
        double prn2 = 34.5;
        double res1 = prn1 + prn2;   // 4=> Après une promotion le résultat aura le même type
        System.out.println(prn1 + " " + prn2 + " " + res1);

        // 2=> La valeur entière est promue en virgule flottant
        int prn3 = 11;
        double res2 = prn2 + prn3;
        System.out.println(prn2 + " " + prn3 + " " + res2);

        int res3 = prn3 / 2; // 5
        double res4 = prn3 / 2.0; // 5.5
        System.out.println(res3 + " " + res4);

        // 3=> byte, short, char sont promus en int
        short s1 = 12;
        short s2 = 30;
        int s3 = s1 + s2;
        System.out.println(s3);

        // Saisie dans la console => Scanner
        Scanner scan = new Scanner(System.in);
        int si = scan.nextInt();
        String str=scan.next();
        System.out.println(si + " " + str);

        // Exercice Somme
        // Saisir 2 chiffres et afficher le résultat dans la console
        // sous la forme 1 + 3 = 4
        System.out.println("Entrer 2 entiers");
        int v1=scan.nextInt();
        int v2=scan.nextInt();
        int add= v1+v2;
        System.out.println(v1 + " + "+ v2 + " = " + add);

        // Exercice Moyenne
        // Saisir 2 chiffres et afficher la moyenne des 2 nombres dans la console
        int va =  scan.nextInt();
        int vb =  scan.nextInt();
        double moy = (va + vb) / 2.0; // ou ((double)(va+vb))/2
        System.out.println("moyenne=" + moy);
        
        scan.close();   // Lorsque l'on ferme le scanner le scanner, on peut plus faire de saisie au clavier (même si l'on recrée un nouvel objet scanner)
                        // => le flux d'entré System.in est fermé
    }
}
