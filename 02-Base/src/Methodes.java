
public class Methodes {

    public static void main(String[] args) {
     // Appel d'une méthode
     double res=mutiplier(2.0,4.0);
     // Appel d'une méthode sans type retour (void)
     afficher(res);
     int vi=12;
     System.out.println(testZero(vi));
     
     // Test des arguments par valeurs
     testParam(vi); // La valeur contenu dans vi est copier dans le paramètre (a)
     System.out.println(vi); // le contenu de vi ne peut pas être modifié par la méthode => passage par valeur
     
     // Exercice: appel de la méthode maximum
     System.out.println( maximum(3,7));
     System.out.println( maximum(23,7));
     
     // Exercice: appel de la méthode paire
     System.out.println( even(8));
     System.out.println( even(5));
     
     // Exercice: tableau
     int[] t= {1,5,6,3};
     afficherTableau(t);
     System.out.println(maximumTableau(t));
     
     // Nombre d'arguments variable
     System.out.println(moyenne(2));
     System.out.println(moyenne(2,3));
     System.out.println(moyenne(2,3,4,8));
     
     // Surcharge de méthode
     System.out.println(somme(1,2));
     System.out.println(somme(3.5,7.8));
     System.out.println(somme(6,5.8));
     System.out.println(somme("hello","world"));
     
     // Si le compilateur ne trouve pas de corresponcance exacte avec le type des paramètres
     // il effectue des conversions automatique pour trouver la méthode à appelé
     System.out.println(somme(1,3L));   // appel de la méthode => somme(double, double )
     System.out.println(somme(1L,3L));  // appel de la méthode => somme(double, double )
     System.out.println(somme('A','A'));    // appel de la méthode => somme(double, double )
   
     // Si le compilateur ne trouve pas de méthode correspondante aux paramètres => ne compile pas
     // System.out.println(somme(1,"world"));
     
     // Affichage des paramètres passés à la méthode main
     for(String arg : args) {
         System.out.println(arg);
     }
     
     // Méthode Récursive
     int f=factorial(3);
     System.out.println(f);
    }
    
    // Déclaration d'une méthode
    static double mutiplier(double d1,double d2) {
        return d1*d2;   // return => Interrompt l'exécution de la méthode
                        //        => Retourne la valeur (expression à droite)
    }
    
   static void afficher(double a){
        System.out.println(a);
        //return;   // avec void => return; ou return peut être omis
    }
   
   static boolean testZero(int v) {
       if(v==0) {
           return true; // Une méthode doit au moins contenir un return (sauf si le type retour est void)
       }
       else {
           return false; // Une méthode peut  contenir plusieurs return
       }
   }
   
   // Test des arguments par valeurs
   static void testParam(int a) {
       System.out.println(a);
       a=123;   // la modification du paramètre a n'a pas de répercution en dehors de la méthode
       System.out.println(a);
   } // à la fin du bloc de la méthode, tous les paramètres (a) et les variables locales sont supprimées (t) 
   
   // Exercice Maximum
   // Écrire une fonction maximum qui prends en paramètre 2 nombres et retourne
   // le maximum
   // Saisir 2 nombres et afficher le maximum entre ces 2 nombres
   static int maximum(int i1, int i2) {
       if(i1>i2) {
           return i1;
       }else {
           return i2;
       }
       // ou
       //     return i1>i2 ? i1 : i2; 
   }
   
   // Exercice Paire
   // Écrire une fonction __Even__ qui prend un entier en paramètre   
   // elle retourne vrai si il est paire
   static boolean even(int valeur) {
       if(valeur%2==0) { // ou valeur & 1 ==0
           return true;
       }else {
           return false;
       }
   }

   // Exercice Tableau
   // - Ecrire un méthode qui affiche un tableau d’entier passé en paramètre
   // - Ecrire une méthode qui calcule le maximum
   static void afficherTableau(int[] tab) {
       System.out.print("[ ");
       for(int e : tab) {
           System.out.print(e + " ");
       }
       System.out.println("]");
   }
   
   static int maximumTableau(int[] tab) {
       int max=tab[0];
       for(int v :tab) {
           if(v>max) {
               max=v;
           }
       }
     return max;
   }
   
   // Nombre d'arguments variable
   static double moyenne(int v1,int... valeurs) {
       // dans la méthode valeurs est considérée comme un tableau
       double somme=v1;
       for(int e : valeurs) {
           somme+=e;
       }
       return somme/(valeurs.length+1);
   }
   
   // Surcharge de méthode
   // Une méthode peut être surchargée => plusieurs méthodes peuvent avoir le même
   // nom, mais leurs signatures doient être différentes
   // La signature d'une méthode correspond aux types et nombre de paramètres
   // Le type de retour ne fait pas partie de la signature

   static int somme(int a,int b) {
       System.out.println("2 entiers");
       return a+b;
   }
   
   static int somme(int v1,int v2,int v3) {
       System.out.println("3 entiers");
       return v1+v2+v3;
   }
   
   static double somme(double d1, double d2) {
       System.out.println("2 doubles");
       return d1+d2;
   }
   
   static double somme(int a, double d) {
       System.out.println("1 entier, 1 double");
       return a+d;
   }
   
   static String somme(String s1, String s2) {
       System.out.println("2 chaines");
       return s1 + s2;
   }
   
   // Méthode récursive
  static  int factorial(int n) { // factoriel= 1* 2* … n
       if (n <= 1) { // condition de sortie
       return 1;
       } else {
       return factorial(n - 1) * n;
       }
 }
}
