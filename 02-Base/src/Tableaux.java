import java.util.Scanner;

public class Tableaux {

    public static void main(String[] args) {
        
        // Déclaration d'un tableau à une dimension
        // double [] tab=null;   // On déclare une référence vers le tableau
        // tab=new double[6];    // On crée l'objet tableau de double 

        double[] tab=new double[6]; // ou double tab[]=new double[6];
        
        // Valeur d'initialisation des éléments du tableau
        // entier -> 0
        // double ou float -> 0.0
        // char -> '\u0000'
        // boolean -> false
        // référence -> null;
        
        // Accèder à un élément du tableau
        tab[0]=1.23; // accès au 1er élément
        System.out.println(tab[0]);
        System.out.println(tab[1]);
        tab[2]=5.5; // accès au 3ème élément
        
        // Taille du tableau
        System.out.println("nb élément tableau="+ tab.length);
        
        // Parcourir un tableau avec un for
        for(int i=0;i<tab.length;i++) {
            System.out.println("tab[" + i + "] =" + tab[i]);
        }
        
        // Parcourir un tableau avec un foreach
        for(double elm : tab) {
            System.out.println(elm);
        }
        
        // Indice en dehors du tableau => génére une exception
        // System.out.println(tab[15]);
        // tab[15]=5.5;
        
        // Déclaration et initialisation du tableau
        char[] tabChr= {'a','z','e','r','t'};
        for(char c:tabChr) {
            System.out.println(c);
        }
        System.out.println(tabChr.length);
      
        // Saisir la taille d tableau
        Scanner scan=new Scanner(System.in);
        int s=scan.nextInt();
        int[] ti=new int[s];
        for(int vi:ti) {
            System.out.println(vi);
        }
        System.out.println(ti.length);
        
        // Exercice Tableau
        // Trouver la valeur maximale et la moyenne d’un tableau de 5 entiers: -7,-4,-8,-10,-3
        int [] t= {-7,-4,-8,-10,-3};
        int max=t[0];
        double somme=0.0;
        for(int v :t) {
            somme+=v;
            if(v>max) {
                max=v;
            }
        }
        System.out.println("maximum=" + max);
        System.out.println("moyenne=" + (somme/t.length));
        
        // Tableau à 2 dimensions
        
        // Déclaration d'un tableau à 2 dimensions
        int[][] tab2d= new int[4][3];
        // ou int tab2d [][]=new char[4][3];
        // ou int [] tab2d []=new char[4][3];

        // Accèder à un élément
        tab2d[1][2]=1;
        System.out.println(tab2d[1][2]);
        
        // Nombre d'élément sur la première dimension => nombre de ligne
        System.out.println(tab2d.length);
        // Nombre de colonne de la première ligne
        System.out.println(tab2d[0].length);
        
        // Parcourir un tableau à 2 dimensions avec un for
        for(int l=0;l<tab2d.length;l++) {
            for(int c=0;c<tab2d[l].length;c++) {
                  System.out.print(tab2d[l][c] + " ");  
            }
            System.out.println("");
        }
        
        // Parcourir un tableau à 2 dimensions avec un foreach
        for( int[] ligne : tab2d) {
            for(int elm :ligne) {
                System.out.print(elm + " ");
            }
            System.out.println("");
        }
        
        // Déclaration et initialisation d'un tableau en 2D
        String [][] tabStr= {{"aze","rty"},{"dfg","uio"},{"ghj","klf"}};
        
        for( String[] ligne : tabStr) {
            for(String elm :ligne) {
                System.out.print(elm + " ");
            }
            System.out.println("");
        }
        
        
        // Tableau à 3 dimensions
        int [][][] tab3d=new int [2][4][3];
        scan.close();
        
        // Accèder à un élément
        tab3d[1][2][0]=12;
        System.out.println(tab3d[1][2][0]);
        
        for (int tb[][] : tab3d) {
            for (int tc[] : tb) {
                for (int b : tc) {
                    System.out.println(b);
                }
            }
        }

        // Tableau en escalier
        
        // Déclaration d'un tableau en escalier => chaque ligne a un nombre d'élement différent
        int [][] tabEsc=new int[3][];
        tabEsc [0]=new int[4];
        tabEsc [1]=new int[2];
        tabEsc [2]=new int[5];
        
        // Accèder à un élément
        tabEsc[2][1]=4;
        
        // Parcourir un Tableau en escalier avec un for
        for ( int l=0;l<tabEsc.length;l++) {
            for(int c=0;c<tabEsc[l].length;c++) {
                System.out.print(tabEsc[l][c]+" ");
            }
            System.out.println("");
        }
        
        // Parcourir un Tableau en escalier avec un foreach
        for( int[] ligne : tabEsc) {
            for(int elm :ligne) {
                System.out.print(elm + " ");
            }
            System.out.println("");
        }
    }

}
