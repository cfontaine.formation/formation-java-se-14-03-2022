package fr.dawan.formation;

import java.util.Comparator;

public class UserComparator implements Comparator<User> {

    @Override
    public int compare(User o1, User o2) {
        // Comparaison pour avoir un classement dans l'ordre alphabétique inversé
        return -((User) o1).getNom().compareTo(((User) o2).getNom());
    }

}
