package fr.dawan.formation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class Application {

    public static void main(String[] args) {
        // Avant java SE 5 => on pouvait stocker tous les objets qui héritent de Object
        // dans une collection
        List lst1 = new ArrayList(); // List => interface , ArrayList =>objet "réel" qui implémente l'interface
        lst1.add("AZERTY"); // add => ajouter un objet à la collection
        lst1.add(123); // Autoboxing type primitf-> type envelope
                       // 123 devient automatiquement un obbjet Integer => new Integer(123)

        lst1.add(3.5);
        // get => récupérer un objet stocké dans la liste à l'indice 0
        // retourne des objects, il faut tester si l'objet correspont à la classe et le
        // caster
        if (lst1.get(0) instanceof String) {

            String str = (String) lst1.get(0);
        }

        // Suppprimer l'élement à l'index 1
        lst1.remove(1);

        // Parcourir une collection avec Iterateur
        Iterator iter = lst1.iterator();
        while (iter.hasNext()) {
            System.out.println(iter.next());
        }

        // Type Générique
        // Classe générique
        Box<String> b1 = new Box<>("Hello");
        Box<Integer> b2 = new Box<>(34);

        // Méthode Générique
        // Pas besoin de préciser le type, il est déduit à partir du type des paramètres
        System.out.println(egal(4.5,4.5) );
        System.out.println(egal('a','z'));
        System.out.println(egal("Hello","Bonjour") );

        // Java 5 => les collection utilise les types générique
        List<String> lstStr = new ArrayList<>(); // la liste ne peut plus contenir que des chaines de caractères
        lstStr.add("Hello");
        lstStr.add(0, "aaaaaa"); // insertion à l'index 0 uniquement pour les listes
        lstStr.add("World");
        lstStr.add("Bonjour");
        lstStr.add("asupprimerindex");
        lstStr.add("asupprimer");

        // lstStr.add(12); // => Erreur

        // get => revoie l'élément placé à l'index
        System.out.println(lstStr.get(0)); // Hello

        // size => le nombre d'élément de la collection
        System.out.println(lstStr.size()); // 3

        // set => remplace l'élément à l'index par celui passé en paramètre
        lstStr.set(1, "Monde");

        lstStr.remove("asupprimer"); // supprime le premier élément "asupprimer"
        lstStr.remove(3); // supprimer l'élément à l'index 3

        // Parcourir une collection "foreach"
        for (String s : lstStr) {
            System.out.println(s);
        }

        // Set=> pas de doublons
        Set<Double> st1 = new HashSet<>();
        st1.add(12.4);
        st1.add(3.8);
        System.out.println(st1.add(4.5)); // true
        // add retourne false => 12.4 existe déjà dans la collection
        System.out.println(st1.add(12.4)); // false

        for (double d : st1) {
            System.out.println(d);
        }

        // SortedSet => tous les objets sont automatiquement triés lorsqu'ils sont
        // ajouté
        // Set trié => interface Comparator

        // On peut utiliser une classe anonyme
//        SortedSet<User> stUser = new TreeSet<>(new Comparator<User>() {
//
//            @Override
//            public int compare(User o1, User o2) {
//                // Comparaison pour avoir un classement dans l'ordre alphabétique inversé
//                return -((User) o1).getNom().compareTo(((User) o2).getNom());
//            }
//        });

        // ou créer une classe qui implémente l'interface Comparator
        SortedSet<User> stUser = new TreeSet<>(new UserComparator());

        stUser.add(new User("John", "Doe"));
        stUser.add(new User("Alan", "Smithee"));
        stUser.add(new User("Jo", "Dalton"));

        for (User u : stUser) {
            System.out.println(u);
        }

        // Set trié => interface Comparable
        SortedSet<User> stUser2 = new TreeSet<>();
        stUser2.add(new User("John", "Doe"));
        stUser2.add(new User("Alan", "Smithee"));
        stUser2.add(new User("Jo", "Dalton"));
        stUser2.add(new User("Jane", "Doe"));

        for (User u : stUser2) {
            System.out.println(u);
        }

        // Queue => file d'attente
        Queue<Double> pileFifo = new LinkedList<>();
        pileFifo.offer(3.4);
        pileFifo.offer(4.9);
        pileFifo.offer(54.7);
        System.out.println(pileFifo.peek());
        System.out.println(pileFifo.peek());
        System.out.println(pileFifo.poll());
        System.out.println(pileFifo.peek());

        // Map => association Clé/Valeur 
        Map<String, User> m = new HashMap<>();
        m.put("fr-59-1", new User("John", "Doe"));      // put => ajouter la clé "fr59-001" et la valeur associé (objet User)
        m.put("fr-59-2", new User("Jane", "Doe"));
        m.put("fr-59-3", new User("Jo", "Dalton"));
        m.put("fr-59-4", new User("Alan", "Smithee"));

        // get -> Obtenir la valeur associé à la clé fr59-003
        System.out.println(m.get("fr-59-2"));
        System.out.println(m.containsKey("fr-62-1")); // false,la clé fr-62-1 n'est pas présente dans la map
        System.out.println(m.containsValue(new User("Jo", "Dalton"))); // true,la valeur est présente dans la map
                                                                       // la comparaisson se fait avec equals
        // si existe déjà, elle est écrasée par la nouvelle valeur associé à la clé 
        m.put("fr-59-3", new User("Marcel", "Dupond"));

        Set<String> keys = m.keySet();  // keySet => retourne toutes les clés de la map
        for (String k : keys) {
            System.out.println(k);
        }

        Collection<User> users = m.values(); // values => retourne toutes les valeurs de la map
        for (User u : users) {
            System.out.println(u);
        }

        // Entry => objet qui contient une clé et la valeur associée 
        Set<Entry<String, User>> ens = m.entrySet();
        for (Entry<String, User> e : ens) {
            System.out.println(e.getKey() + " => " + e.getValue());
        }

        // Classe Collections => Classe utilitaires pour les collections
        System.out.println(Collections.min(lstStr)); // min=> l'élément minimum de la collection 
        
        Collections.sort(lstStr);   // sort => pour trier une liste
        for (String s : lstStr) {   
            System.out.println(s);
        }

     // Classe Arrays  => Classe utilitaires pour les tableaux
        String[] tabStr = new String[5];
        Arrays.fill(tabStr, "Bonjour");     // => initialiser un tableau avec une valeur

        System.out.println(Arrays.toString(tabStr));    // toString =>  afficher un tableau
        int tab1[] = { 1, 5, 7, 3, 8 };
        int tab2[] = { 1, 5, 7, 3, 8 };
        System.out.println(Arrays.equals(tab1, tab2));  // equals => comparaison de deux tableaux
        Arrays.sort(tab2);                              // sort => tri d'un tableau
        System.out.println(Arrays.toString(tab2));

        System.out.println(Arrays.binarySearch(tab2, 5));   // binarySearch => recherche d'un élément dans un tableau trié
        int index = Arrays.binarySearch(tab2, 6);
        System.out.println((index + 1) * -1);
    }

    // Méthode générique
    static <T> boolean egal(T a, T b) {
        return a.equals(b);
    }


}
