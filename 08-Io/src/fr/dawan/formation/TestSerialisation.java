package fr.dawan.formation;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;

public class TestSerialisation {

    public static void main(String[] args) {

        // Sérialisation
        // l'objet Personne va être sérialiser ainsi que tous les objets qu'il contient
        // ObjectOutputStream permet de persiter un objet ou une grappe d'objet
        Contact c = new Contact("John", "Doe", "jdoe@dawan.com", LocalDate.of(1996, 4, 23));
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("contact.dat"))) {
            oos.writeObject(c);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Désérialisation
        // ObjectInputStream permet de désérialiser
        Object obj = null;
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("contact.dat"))) {
            obj = ois.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        if (obj instanceof Contact) {
            Contact cr = (Contact) obj;
            System.out.println(cr);
        }
    }

}
