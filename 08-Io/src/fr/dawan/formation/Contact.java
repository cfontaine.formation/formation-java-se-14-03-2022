package fr.dawan.formation;

import java.io.Serializable;
import java.time.LocalDate;

//Pour qu'une classe soit sérialisable, elle doit implémenter l'interface Serializable
public class Contact implements Serializable {

    // serialVersionUID est une clé de hachage SHA qui identifie de manière unique
    // la classe.
    // Si la classe personne évolue et n'est plus compatible avec les objets
    // précédamment persistés, on modifie la valeur du serialVersionUID
    // et lors de la désérialisation une exception sera générée pour signaler
    // l'incompatibilité
    // java.io.InvalidClassException: fr.dawan.formation.Contact; local class
    // incompatible: stream classdesc serialVersionUID = 1, local class
    // serialVersionUID = 2
    // si l'on fournit pas serialVersionUID le compilateur va en générer un (à
    // éviter).
    private static final long serialVersionUID = 1L;

    private long id;

    private String prenom;

    private String nom;

    private String email;

    // Si un attribut ne doit pas être sérialiser on ajout le mot clef transient
    private transient LocalDate dateNaissance;

    private static int cpt; // les variable de classe ne sont pas sérialisée
    
    public Contact() {
        cpt++;
    }

    public Contact(String prenom, String nom, String email, LocalDate dateNaissance){
        this();
        this.prenom = prenom;
        this.nom = nom;
        this.email = email;
        this.dateNaissance = dateNaissance;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public static int getCpt() {
        return cpt;
    }

    public static void setCpt(int cpt) {
        Contact.cpt = cpt;
    }

    @Override
    public String toString() {
        return "Contact [id=" + id + ", prenom=" + prenom + ", nom=" + nom + ", email=" + email + ", dateNaissance="
                + dateNaissance + "]";
    }
}
