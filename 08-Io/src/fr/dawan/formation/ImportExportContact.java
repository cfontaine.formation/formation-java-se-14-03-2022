package fr.dawan.formation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

// Exercice: Import/Export CSV
// Dans une classe ImportExportContact
// Faire 2 méthodes de classe:
//  - public void exportCsv(String path, List<Contact> contacts)
//  - public static List<Contact> importCsv(String path
//
// Le format d'une ligne de fichier : id;prenom;nom;email;dateNaissance

//la classe Contact a pour attribut:
//- id (long)
//- prenom
//- nom
//- email
//- dateNaissance

public class ImportExportContact {

    public static void exportCsv(String path, List<Contact> contacts) throws IOException {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(path))) {
            for (Contact c : contacts) {
                bw.write(contactToString(c, ";"));
                bw.newLine();
            }
        }
    }

    public static List<Contact> importCsv(String path) throws IOException {
        List<Contact> lstEmploye = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            String line = null;
            while (true) {
                line = br.readLine();
                if (line == null) {
                    break;
                }
                Contact tmp = stringToContact(line, ";");
                if (tmp != null) {
                    lstEmploye.add(tmp);
                }
            }
        }
        return lstEmploye;
    }

    private static String contactToString(Contact contact, String separator) {
        StringBuilder sb = new StringBuilder();
        sb.append(contact.getId());
        sb.append(separator);
        sb.append(contact.getPrenom());
        sb.append(separator);
        sb.append(contact.getNom());
        sb.append(separator);
        sb.append(contact.getEmail());
        sb.append(separator);
        sb.append(contact.getDateNaissance());
        return sb.toString();
    }

    private static Contact stringToContact(String line, String separator) {
        Contact c = null;
        StringTokenizer tok = new StringTokenizer(line, separator);
        if (tok.countTokens() == 5) {
            long id = Integer.parseInt(tok.nextToken());
            c = new Contact(tok.nextToken(), tok.nextToken(), tok.nextToken(), LocalDate.parse(tok.nextToken()));
            c.setId(id);
        }
        return c;
    }
}
