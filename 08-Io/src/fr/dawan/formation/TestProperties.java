package fr.dawan.formation;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import java.util.Set;

public class TestProperties {

    public static void main(String[] args) {

        // Ecriture des propriétées

        // Properties représente un ensemble persistant de propriétés (clé=valeur)
        // uniquement des chaines de caractères
        Properties pw = new Properties();
        pw.setProperty("version", "1.0");
        pw.setProperty("user", "janedoe");
        pw.setProperty("asup", "aaaaa");
        // Supression d'une propriété
        pw.remove("asup");

        try {
            // On peut sauver les propriétés :
            // - dans un fichier .properties
            pw.store(new FileWriter("test.properties"), "Commentaire: test fichier properties");
            // - dans un fichier .xml
            pw.storeToXML(new FileOutputStream("test.xml"), "Commentaire: test fichier de proriété en xml", "UTF-8");
            
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        pw.clear(); // effacer toutes les propriétés de l'objet Properties

        // Lecture des propriétées
        Properties pr = new Properties();
        try {
            // Chargement du fichier 
            // - à partir d'un fichier .properties
            pr.load(new FileReader("test.properties"));
            // - à partir d'un fichier .xml
            pr.loadFromXML(new FileInputStream("test.xml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        // Récupérer la valeur à partir de la clé
        System.out.println(pr.getProperty("version"));
        System.out.println(pr.getProperty("user"));
        System.out.println(pr.getProperty("existepas")); // si la clé n'existe pas => null
        System.out.println(pr.getProperty("existepas", "valeur default")); // si la clé n'existe pas => valeur default

        // Récupérer un set contenant toutes les clés
        Set<Object> keys = pr.keySet();
        for (Object k : keys) {
            System.out.println("Clef=" + k);
        }

    }

}
