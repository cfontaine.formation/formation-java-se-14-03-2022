package fr.dawan.formation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class TestFichier {

    public static void main(String[] args) {
        // File.séparator Le caractère de séparation par défaut dépendant du système
        System.out.println(File.separatorChar);

        // Fichier texte
        ecrireFichierText("test.txt");
        ecrireFichierTextBuff("test2.txt");
        ecrireFichierTextPrint("test3.txt");
        lireFichierText("test.txt");

        List<String> line = lireFichierTextBuff("test2.txt");
        for (String s : line) {
            System.out.println(s);
        }

        // Parcourir le répertoire du projet 03-Poo
        // File : représentation des chemins d'accès aux fichiers et aux répertoires
        parcourir(new File("..\\03-Poo"));

        // Fichier binaire
        // Exercice Copie de fichier
        copier("logo.png", "logo_copie.png");
        
        // Exercice Import/Export Csv
        List <Contact> contacts=new ArrayList<Contact>();
        contacts.add(new Contact("John","Doe","jdoe@dawan.com",LocalDate.of(1997, 12, 9)));
        contacts.add(new Contact("Jane","Doe","jadoe@dawan.com",LocalDate.of(1999, 1, 19)));
        contacts.add(new Contact("Alan","Smithee","asmithee@dawan.com",LocalDate.of(1981, 6, 12)));
        contacts.add(new Contact("Jo","Dalton","jdalton@dawan.com",LocalDate.of(1989, 3, 14)));
        
        try {
            ImportExportContact.exportCsv("contact.csv", contacts);
            List<Contact> cts=ImportExportContact.importCsv("contact.csv");
            for(Contact c: cts) {
                System.out.println(c);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void ecrireFichierText(String path) {
        FileWriter fw = null;
        try {
            fw = new FileWriter(path); // Ouverture du flux
            for (int i = 0; i < 10; i++) {
                fw.write("Bonjour"); // Ecriture d'une chaine dans le fichier
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fw != null) {
                try {
                    fw.close(); // Fermeture du flux
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // Utilisation de try with ressource pour fermer automatiquement le flux vers le fichier (à partir du java 7)
    // try with ressource fonctionne avec tous les objets implémentant l'interface AutoCloseable
    public static void ecrireFichierTextBuff(String path) {
        // On utilise un BufferedWriter, il n'a pas accès directement au fichier, il
        // faut passer par un FileWriter
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(path, true))) { // append => true : on ajoute des données au fichier
                                                                                   //           false : lefichier est écrasé (par défaut)
            for (int i = 0; i < 10; i++) {
                bw.write("Bonjour");
                bw.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Ecrire dans un fichier texte avec PrintWriter
    public static void ecrireFichierTextPrint(String chemin) {
        try (PrintWriter pw = new PrintWriter(new FileWriter(chemin, true))) {
            for (int i = 0; i < 10; i++) {
                pw.println(i);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Lecture d'un fichier texte
    public static void lireFichierText(String chemin) {
        char[] buff = new char[10];
        try (FileReader fr = new FileReader(chemin)) {
            while (fr.read(buff) > 0) { // lecture de 10 caractères maximum dans le fichier
                                        // lorsque l'on atteint la fin du fichier read retourne -1
                for (char c : buff) {
                    System.out.print(c);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Lecture d'un fichier texte avec BufferedReader
    public static List<String> lireFichierTextBuff(String path) {
        List<String> lst = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            for (;;) { // boucle infinie
                String row = br.readLine(); // lecture d'une ligne dans le fichier
                if (row == null) { // lorsque l'on atteint la fin du fichier readLine retourne null
                    break;
                }
                lst.add(row);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lst;
    }

    // Parcourir un système de fichiers
    public static void parcourir(File f) {
        if (f.exists()) { // Test l'existence du fichier ou du dossier
            if (f.isDirectory()) { // si c'est un dossier
                System.out.println("");
                System.out.println("> Répertoire= " + f.getName()); // affichage du nom du répertoire

                File[] tf = f.listFiles(); // Récupération du contenu du dossier
                for (File fi : tf) {
                    parcourir(fi); // Appel récursif sur chaque fichier du dossier
                }
            } else {
                System.out.println("  " + f.getName()); // affichage du nom du fichier
            }
        } else {
            f.mkdir(); // si le chemin n'existe pas on crée un dossier
        }
    }

    // Exercice copie : faire une méthode qui va copier un fichier octet par octet
    public static void copier(String pathSource, String pathTarget) {
        try (FileInputStream fis = new FileInputStream(pathSource);
                FileOutputStream fos = new FileOutputStream(pathTarget)) {
            while (true) {
                int b = fis.read();
                if (b == -1) {
                    break;
                }
                fos.write(b);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
