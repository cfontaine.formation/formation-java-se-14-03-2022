package fr.dawan.formation;

public class Terrain {

    public static final int NB_MAX_FORME = 10;

    private Forme tabForme[] = new Forme[NB_MAX_FORME];
    private int nbForme;

    public Terrain() {

    }

    public void addForme(Forme forme) {
        if (nbForme < tabForme.length && forme != null) {
            tabForme[nbForme] = forme;
            nbForme++;
        }
    }

    public double surfaceTotal() {
        double total = 0.0;
        for (int i = 0; i < nbForme; i++) {
            total += tabForme[i].calculSurface();
        }
        return total;
    }

    public double surfaceTotal(Couleur co) {
        double total = 0.0;
        for (int i = 0; i < nbForme; i++) {
            if (tabForme[i].getCouleur() == co) {
                total += tabForme[i].calculSurface();
            }
        }
        return total;
    }
}
