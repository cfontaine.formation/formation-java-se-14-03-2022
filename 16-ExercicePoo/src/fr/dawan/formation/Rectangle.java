package fr.dawan.formation;

public class Rectangle extends Forme {

    private double largeur;

    private double longueur;

    public Rectangle(Couleur couleur, double largeur, double longueur) {
        super(couleur);
        this.largeur = largeur;
        this.longueur = longueur;
    }

    public double getLargeur() {
        return largeur;
    }

    public void setLargeur(double largeur) {
        this.largeur = largeur;
    }

    public double getLongueur() {
        return longueur;
    }

    public void setLongueur(double longueur) {
        this.longueur = longueur;
    }

    @Override
    public double calculSurface() {
        return largeur * longueur;
    }

    @Override
    public String toString() {
        return "Rectangle [largeur=" + largeur + ", longueur=" + longueur + ", couleur=" + getCouleur() + "]";
    }

}
