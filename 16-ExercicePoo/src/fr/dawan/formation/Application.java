package fr.dawan.formation;

public class Application {

    public static void main(String[] args) {
        Terrain t = new Terrain();
        t.addForme(new Rectangle(Couleur.BLEU, 2.0, 2.0));
        t.addForme(new Rectangle(Couleur.BLEU, 2.0, 2.0));
        t.addForme(new Rectangle(Couleur.BLEU, 2.0, 2.0));
        t.addForme(new TriangleRectangle(Couleur.VERT, 2.0, 2.0));
        t.addForme(new TriangleRectangle(Couleur.VERT, 2.0, 2.0));
        t.addForme(new Cercle(Couleur.ROUGE, 1.0));
        t.addForme(new Cercle(Couleur.ROUGE, 1.0));
        t.addForme(new Cercle(Couleur.ORANGE, 1.0));
        t.addForme(new Rectangle(Couleur.ORANGE, 1.0, 1.0));

        System.out.println("Surface total=" + t.surfaceTotal());
        System.out.println("Surface BLEU=" + t.surfaceTotal(Couleur.BLEU));
        System.out.println("Surface VERT=" + t.surfaceTotal(Couleur.VERT));
        System.out.println("Surface ROUGE=" + t.surfaceTotal(Couleur.ROUGE));
        System.out.println("Surface ORANGE=" + t.surfaceTotal(Couleur.ORANGE));
    }

}
