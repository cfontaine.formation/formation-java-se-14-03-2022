package fr.dawan.formation;

public class TriangleRectangle extends Rectangle {

    public TriangleRectangle(Couleur couleur, double largeur, double longueur) {
        super(couleur, largeur, longueur);
    }

    @Override
    public double calculSurface() {
        return super.calculSurface() / 2;
    }

}
