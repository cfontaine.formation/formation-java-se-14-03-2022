package fr.dawan.formation;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Date;
import java.util.StringTokenizer;

public class Application {

    public static void main(String[] args) {
        // Enumération
        Motorisation m = Motorisation.ELECTRIQUE;
        // Enum -> String
        // name et toString => chaîne de caractères contenant le nom de la constante
        String str = m.name();
        System.out.println(str);
        System.out.println(m);

        // String -> Enum
        Motorisation m2 = Enum.valueOf(Motorisation.class, "GPL");
        // Motorisation m3 = Enum.valueOf(Motorisation.class, "GPL2"); // Erreur Lavaleur GPL2 n'existe pas dans l'enumération
        System.out.println(m2);

        // ordinal -> l'index de la valeur selon l'ordre de déclaration (commence à partir de 0)
        System.out.println(m2.ordinal()); // 2

        // values -> tableau de toutes les valeurs énumérées disponibles
        Motorisation[] ms = Motorisation.values();
        for (Motorisation mo : ms) {
            System.out.println(mo);
        }
        

        // Chaine de caractères
        String str1 = "Hello World";
        String str2 = new String("Bonjour");

        // Les chaines de caractères sont immuables une fois créée elles ne peuvent plus
        // être modifiées
        str1.toLowerCase();
        str1.substring(4);
        str1.trim();
        System.out.println(str1);

        // égalité de 2 chaines
        System.out.println(str1.equals("Hello World")); // true
        System.out.println(str1.equals(str2)); // false

        // Comparaison 0-> égale, >0 -> se trouve après, <0 -> se trouve avant dans
        // l'ordre alphabétique
        // valeur retournée: distance entre les 2 chaine B -> H =6
        System.out.println(str1.compareTo(str2)); // >0 après
        System.out.println(str2.compareTo(str1)); // <0 avant
        System.out.println(str2.compareTo("Bonjour")); // 0 ==

        // length -> Nombre de caractère de la chaine de caractère
        System.out.println(str1.length());

        // Concaténation
        System.out.println(str1 + str2); // + -> concaténation
        System.out.println(str1.concat(str2));
        System.out.println(str1);

        // La méthode join concatène les chaines, en les séparants par une chaine de
        // séparation
        String str3 = String.join(";", "azerty", "yuiop", "fghjkl");
        System.out.println(str3); // azerty;yuiop;fghjkl

        // Découpe la chaine et retourne un tableau de sous-chaine suivant un séparateur
        String[] tabStr = str3.split(";");
        for (String s : tabStr) {
            System.out.println(s);
        }

        // substring permet d'extraire une sous-chaine
        // de l'indice passé en paramètre jusqu'à la fin de la chaine
        System.out.println(str1.substring(6)); // World
        // de l'indice passé en paramètre jusqu'à l'indice de fin (exclut)
        System.out.println(str1.substring(6, 9)); // Wor

        // startsWith retourne true, si la chaine commence par la chaine passé en
        // paramètre
        System.out.println(str1.startsWith("Hello")); // true
        System.out.println(str1.startsWith("aaaaa")); // false

        // indexOf retourne la première occurence de la chaine passée en paramètre
        System.out.println(str1.indexOf("o")); // 4
        System.out.println(str1.indexOf("o", 5)); // idem mais à partir de l'indice passé en paramètre 7
        System.out.println(str1.indexOf("o", 8)); // retourne -1, si le caractère n'est pas trouvé

        // Remplace toutes les les sous-chaines target par replacement
        System.out.println(str1.replace("o", "a")); // Hella Warld

        // Retourne le caractère à l'indice 3
        System.out.println(str1.charAt(3)); // l

        // Retourne true, si la sous-chaine passée en paramètre est contenu dans la
        // chaine
        System.out.println(str1.contains("lo")); // true
        System.out.println(str1.contains("aaaa")); // false

        // Retourne la chaine en majuscule
        System.out.println(str1.toUpperCase()); // HELLO WORLD
        System.out.println("aaaa".toUpperCase()); // AAAA

        // trim supprime les caractères de blanc du début et de la fin de la chaine
        String str4 = "\t \n   \t Hello World  \t \n \n";
        System.out.println(str4);
        System.out.println(str4.trim());

        // Retourne une chaine formater
        // %d -> entier, %s -> String, %f -> réel, %c -> caratère ...
        System.out.println(String.format("%d [%s]  =>%f", 42, "Bonjour", 12.3));

        // On peut chainer les méthodes
        String str5 = str1.substring(6).toUpperCase().concat(" hello").toLowerCase();
        System.out.println(str5);

        // StringBuilder
        // Lorsqu'il y a beaucoup de manipulation (+de 3) sur une chaine ( concaténation
        // et insertion, remplacement,supression de sous-chaine)
        // il vaut mieux utiliser un objet StringBuilder qui est plus performant qu'un
        // objet String => pas de création d'objet intermédiare
        StringBuilder sb = new StringBuilder("hello");
        sb.append(" ");
        sb.append("world");
        sb.append(false);
        sb.append(34);
        sb.delete(5, 6);
        sb.insert(5, "______________");
        String str6 = sb.toString();
        System.out.println(str6);

        // StringTokenizer
        // Permet de décomposer une chaîne de caractères en une suite de mots séparés
        // par des délimiteurs
        StringTokenizer st = new StringTokenizer("azeerty;yuiop;fghjk;xcvbn", ";");
        System.out.println(st.countTokens());
        while (st.hasMoreTokens()) {
            System.out.println(st.nextToken());
        }

        // Excercice Chaine de caractère
        System.out.println(inverser("Bonjour"));
        System.out.println(palindrome("Radar"));
        System.out.println(palindrome("SOS"));
        System.out.println(palindrome("Bonjour"));
        

        // Date
        Date d = new Date();
        System.out.println(d);

        // à partir de java 8 => LocalDate, LocalTime, LocalDateTime
        LocalDate dateDuJour = LocalDate.now(); // now => obtenir la date courante
        System.out.println(dateDuJour);
        
        LocalTime currentTime=LocalTime.now(); // heure courante
        System.out.println(currentTime);

        //of => créer une date spécifique
        LocalDate dernierJour2022 = LocalDate.of(2022, Month.DECEMBER, 31); // 12
        System.out.println(dernierJour2022);

        // On manipule une date avec les méthodes plusXXXXX et minusXXXXX
        LocalDate d1 = dateDuJour.plusYears(1).plusMonths(5).minusDays(6); // 12/08/2023
        System.out.println(d1);

        // Period => représente une durée
        Period p = Period.ofWeeks(3);
        LocalDate d2 = dateDuJour.plus(p); // plus et minus permettent d'ajouter ou retirer une Period
        System.out.println(d2);
        // between -> obtenir une durée entre 2 dayes
        Period p2 = Period.between(dateDuJour, dernierJour2022);
        System.out.println(p2);

        // LocalDate -> String
        System.out.println(dateDuJour.format(DateTimeFormatter.ISO_LOCAL_DATE));
        System.out.println(dateDuJour.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)));
        System.out.println(dateDuJour.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)));
        System.out.println(dateDuJour.format(DateTimeFormatter.ofPattern("dd/MM/YY")));
        System.out.println(dateDuJour.format(DateTimeFormatter.ofPattern("dd MMMM YYYY")));

        // String -> LocalDate
        LocalDate d3 = LocalDate.parse("2022-04-04", DateTimeFormatter.ISO_LOCAL_DATE);
        System.out.println(d3);

        // Heure en fonction des fuseaux horraires
        LocalTime t1 = LocalTime.now(ZoneId.of("GMT+3"));
        System.out.println(t1);
    }
    
//    Exercice Inversion de chaine
//    Écrire la méthode Inverser qui prend en paramètre une chaine et qui retourne la chaine avec les caractères inversés  
//    exemple : bonjour => ruojnob
    static String inverser(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = str.length() - 1; i >= 0; i--) {
            sb.append(str.charAt(i));
        }
        return sb.toString();
    }

//    Exercice Palindrome
//    Écrire une méthode qui accepte en paramètre une chaine et qui retourne un booléen pour indiquer si c'est palindrome
//  exemple : SOS, radar
    static boolean palindrome(String str) {
        String tmp = str.trim().toLowerCase();
        return tmp.equals(inverser(tmp));
    }

}
